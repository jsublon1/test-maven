/*==============================================================*/
/* Nom de SGBD :  MySQL 5.0                                     */
/* Date de création :  25/01/2023 11:44:00                      */
/*==============================================================*/

drop database if exists ecole;

create database if not exists ecole;

use ecole;

drop table if exists adherent;

drop table if exists admin;

drop table if exists adresse;

drop table if exists compose;

drop table if exists ecole;

drop table if exists est;

drop table if exists groupe;

drop table if exists instrument;

drop table if exists joue;

drop table if exists role;

drop table if exists style;

drop table if exists type;

/*==============================================================*/
/* Table : adresse                                              */
/*==============================================================*/
create table adresse
(
   adresse_id           int not null auto_increment,
   adresse_num          char(5) not null,
   adresse_voie         char(255) not null,
   adresse_codepostal   char(5) not null,
   adresse_ville        char(255) not null,
   primary key (adresse_id)
);

/*==============================================================*/
/* Table : ecole                                                */
/*==============================================================*/
create table ecole
(
   ecole_id             int not null auto_increment,
   adresse_id           int not null,
   ecole_nom            char(255) not null,
   ecole_email          char(255) not null,
   ecole_telephone      char(10) not null,
   primary key (ecole_id),
   constraint fk_se_situe foreign key (adresse_id)
      references adresse (adresse_id) on delete restrict on update restrict
);

/*==============================================================*/
/* Table : adherent                                             */
/*==============================================================*/
create table adherent
(
   adherent_id          int not null auto_increment,
   adresse_id           int not null,
   ecole_id             int not null,
   adherent_nom         char(255) not null,
   adherent_prenom      char(255) not null,
   adherent_email       char(255) not null,
   adherent_telephone   char(10) not null,
   primary key (adherent_id),
   constraint fk_habite foreign key (adresse_id)
      references adresse (adresse_id) on delete restrict on update restrict,
   constraint fk_possede foreign key (ecole_id)
      references ecole (ecole_id) on delete restrict on update restrict
);

/*==============================================================*/
/* Table : role                                                 */
/*==============================================================*/
create table role
(
   role_id              int not null auto_increment,
   role_nom           char(20) not null,
   primary key (role_id)
);

/*==============================================================*/
/* Table : admin                                                */
/*==============================================================*/
create table admin
(
   admin_id             int not null auto_increment,
   role_id              int not null,
   admin_pseudo         char(255) not null unique,
   admin_motdepasse     char(255) not null,
   admin_email          char(255) not null unique,
   admin_cookietoken    char(255) ,
   admin_cookietokenexpiration date ,
   primary key (admin_id),
   constraint fk_role foreign key (role_id)
      references role (role_id) on delete restrict on update restrict
);

/*==============================================================*/
/* Table : style                                                */
/*==============================================================*/
create table style
(
   style_id             int not null auto_increment,
   style_nom          char(255) not null,
   primary key (style_id)
);

/*==============================================================*/
/* Table : groupe                                               */
/*==============================================================*/
create table groupe
(
   groupe_id            int not null auto_increment,
   style_id             int not null,
   ecole_id             int not null,
   groupe_nom           char(255) not null,
   groupe_datecreation  date not null,
   primary key (groupe_id),
   constraint fk_est_de foreign key (style_id)
      references style (style_id) on delete restrict on update restrict,
   constraint fk_gere foreign key (ecole_id)
      references ecole (ecole_id) on delete restrict on update restrict
);

/*==============================================================*/
/* Table : compose                                              */
/*==============================================================*/
create table compose
(
   groupe_id            int not null,
   adherent_id          int not null,
   primary key (groupe_id, adherent_id),
   constraint fk_compose2 foreign key (groupe_id)
      references groupe (groupe_id) on delete restrict on update restrict,
   constraint fk_compose foreign key (adherent_id)
      references adherent (adherent_id) on delete restrict on update restrict
);

/*==============================================================*/
/* Table : type                                                 */
/*==============================================================*/
create table type
(
   type_id              int not null auto_increment,
   type_nom           char(255) not null,
   primary key (type_id)
);

/*==============================================================*/
/* Table : est                                                  */
/*==============================================================*/
create table est
(
   adherent_id          int not null,
   type_id              int not null,
   primary key (adherent_id, type_id),
   constraint fk_est2 foreign key (adherent_id)
      references adherent (adherent_id) on delete restrict on update restrict,
   constraint fk_est foreign key (type_id)
      references type (type_id) on delete restrict on update restrict
);

/*==============================================================*/
/* Table : instrument                                           */
/*==============================================================*/
create table instrument
(
   instrument_id        int not null auto_increment,
   instrument_nom       char(255) not null,
   primary key (instrument_id)
);

/*==============================================================*/
/* Table : joue                                                 */
/*==============================================================*/
create table joue
(
   instrument_id        int not null,
   adherent_id          int not null,
   primary key (instrument_id, adherent_id),
   constraint fk_joue2 foreign key (instrument_id)
      references instrument (instrument_id) on delete restrict on update restrict,
   constraint fk_joue foreign key (adherent_id)
      references adherent (adherent_id) on delete restrict on update restrict
);

/*==============================================================*/
/* Tiggers : ADHERENT                                           */
/*==============================================================*/

DELIMITER |
DROP TRIGGER IF EXISTS effacer_adherent|
CREATE TRIGGER effacer_adherent
	BEFORE DELETE ON adherent
	FOR EACH ROW
	BEGIN
		DELETE FROM est WHERE est.adherent_id = old.adherent_id;
        DELETE FROM joue WHERE joue.adherent_id = old.adherent_id;
        DELETE FROM compose WHERE compose.adherent_id = old.adherent_id;
	END|
DELIMITER ;

/*==============================================================*/
/* Tiggers : GROUPE                                         */
/*==============================================================*/

DELIMITER |
DROP TRIGGER IF EXISTS effacer_groupe|
CREATE TRIGGER effacer_groupe
	BEFORE DELETE ON groupe
	FOR EACH ROW
	BEGIN
        DELETE FROM compose WHERE compose.groupe_id = old.groupe_id;
	END|
DELIMITER ;

/*==============================================================*/
/* Tiggers : ECOLE                                         */
/*==============================================================*/

DELIMITER |
DROP TRIGGER IF EXISTS effacer_ecole|
CREATE TRIGGER effacer_ecole
	BEFORE DELETE ON ecole
	FOR EACH ROW
	BEGIN
        DELETE FROM adherent WHERE adherent.ecole_id = old.ecole_id;
		DELETE FROM groupe WHERE groupe.ecole_id = old.ecole_id;
	END|
DELIMITER ;