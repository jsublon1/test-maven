use ecole;

insert into role(role_nom)
values 	("admin"),
		("moderateur");

insert into type(type_nom)
values 	("chanteur"),
		("musicien");

insert into style(style_nom)
values 	("rock"),
		("pop"),
        ("classique"),
        ("reggae"),
        ("metal");

insert into instrument(instrument_nom)
values 	("guitare"),
		("basse"),
        ("batterie"),
        ("piano"),
        ("violon"),
        ("flute"),
        ("harmonica"),
        ("trompette");

insert into admin(role_id,admin_pseudo,admin_motdepasse,admin_email,admin_cookietoken,admin_cookietokenexpiration)
values 	(1,"admin","sha256hex$UN7Xyu96BHh1uhteawsN$cf94610da8fa974ad5c0fdca526bfa7fc709214bf3e18872f3560c40acd6c453", "admin@boss.me", null, null),
		(1,"moi","sha256hex$xyD9Goppa4N7eAxbDFH4$4d43a3bc6267a6ca437272567e866ef30c650c66c29e2309e47ae291cec3355a", "moi@boss.me", null, null);

insert into adresse(adresse_num,adresse_voie,adresse_codepostal,adresse_ville)
values 	("75","boulevard marechal foch","54320", "Laxou"),
		("21","rue loufoque","54120", "Pres-les-Nancy"),
        ("14","avenue la grande","54007", "Bond sur Nancy");

insert into ecole(adresse_id,ecole_nom,ecole_email,ecole_telephone)
values (2, "Les Charlots", "lescharlots@free.fr","0354789426");

insert into adherent(adresse_id,ecole_id,adherent_nom,adherent_prenom,adherent_email,adherent_telephone)
values 	(1,1, "Sublon", "Julien", "js@gmail.fr","0123456789"),
		(3,1, "Bond", "James", "007@gmail.uk","0214789563");
        
insert into groupe(style_id,ecole_id,groupe_nom,groupe_datecreation)
values 	(1,1, "Lady Killers", "2021-11-18");
        
insert into est(adherent_id,type_id)
values 	(1,1),
		(1,2),
        (2,2);
        
insert into joue(adherent_id,instrument_id)
values 	(1,1),
		(1,7),
        (2,3);
        
insert into compose(groupe_id,adherent_id)
values 	(1,1),
		(1,2);

