package beans;

import exceptions.CreationException;

public class Adresse {

	int id;
	String numero, voie, codePostal, ville;

	public Adresse(int id, String num, String voie, String cp, String ville) throws CreationException{
		this.setId(id);
		this.setNumero(num);
		this.setVoie(voie);
		this.setCodePostal(cp);
		this.setVille(ville);
	}
	public Adresse(String num, String voie, String cp, String ville) throws CreationException{
		this.setId(0);
		this.setNumero(num);
		this.setVoie(voie);
		this.setCodePostal(cp);
		this.setVille(ville);
	}
	
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 * @throws CreationException 
	 */
	public void setId(int id) throws CreationException {
		if (id<0) throw new CreationException("L'identifiant est négatif");
		this.id = id;
	}


	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * @param numero the numero to set
	 * @throws CreationException 
	 */
	public void setNumero(String numero) throws CreationException {
		if(numero.trim().isEmpty()||numero==null) 
			throw new CreationException("Le numéro de rue n'est pas indiqué");
		this.numero = numero;
	}

	/**
	 * @return the voie
	 */
	public String getVoie() {
		return voie;
	}

	/**
	 * @param voie the voie to set
	 * @throws CreationException 
	 */
	public void setVoie(String voie) throws CreationException {
		if(voie.trim().isEmpty()||voie==null) 
			throw new CreationException("Le nom de rue n'est pas indiqué");
		this.voie = voie;
	}

	/**
	 * @return the codePostal
	 */
	public String getCodePostal() {
		return codePostal;
	}

	/**
	 * @param codePostal the codePostal to set
	 * @throws CreationException 
	 */
	public void setCodePostal(String codePostal) throws CreationException {
		if(codePostal.trim().isEmpty()||codePostal==null) 
			throw new CreationException("Le code postal n'est pas indiqué");
		if(!codePostal.matches("[0-9]{5}")) 
			throw new CreationException("Le code postal est un code à 5 chiffres");
		this.codePostal = codePostal;
	}

	/**
	 * @return the ville
	 */
	public String getVille() {
		return ville;
	}

	/**
	 * @param ville the ville to set
	 * @throws CreationException 
	 */
	public void setVille(String ville) throws CreationException {
		if(ville.trim().isEmpty()||ville==null) 
			throw new CreationException("La ville n'est pas indiqué");
		this.ville = ville;
	}
	
}
