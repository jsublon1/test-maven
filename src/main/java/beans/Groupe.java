package beans;

import java.time.LocalDate;
import java.util.ArrayList;

import exceptions.CreationException;

public class Groupe {
	
	int id;
	String nom, style;
	LocalDate dateCreation;
	ArrayList<Personne> listeMembres = new ArrayList<Personne>();
	
	public Groupe(int id,String nom, LocalDate date, String style,ArrayList<Personne> listeMembres) throws CreationException
	{
		this.setId(id);
		this.setNom(nom);
		this.setDateCreation(date);
		this.setStyle(style);
		this.setListeMembres(listeMembres);
	}
	public Groupe(String nom, LocalDate date, String style,ArrayList<Personne> listeMembres) throws CreationException
	{
		this.setId(0);
		this.setNom(nom);
		this.setDateCreation(date);
		this.setStyle(style);
		this.setListeMembres(listeMembres);
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 * @throws CreationException 
	 */
	public void setId(int id) throws CreationException {
		if (id<0) throw new CreationException("L'identifiant est négatif");
		this.id = id;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 * @throws CreationException 
	 */
	public void setNom(String nom) throws CreationException {
		if(nom==null || nom.trim().equals("")) throw new CreationException("Le nom du groupe n'est pas indiqué");
		this.nom = nom;
	}
	/**
	 * @return the dateCreation
	 */
	public LocalDate getDateCreation() {
		return dateCreation;
	}
	/**
	 * @param dateCreation the dateCreation to set
	 * @throws CreationException 
	 */
	public void setDateCreation(LocalDate dateCreation) throws CreationException {
		if(dateCreation == null) throw new CreationException("La date de création n'est pas indiqué");
		this.dateCreation = dateCreation;
	}
	/**
	 * @return the listeMembres
	 */
	public ArrayList<Personne> getListeMembres() {
		return listeMembres;
	}
	/**
	 * @param listeMembres the listeMembres to set
	 * @throws CreationException 
	 */
	public void setListeMembres(ArrayList<Personne> listeMembres) throws CreationException {
		if(listeMembres.isEmpty()||listeMembres==null) throw new CreationException("Le groupe de contient aucun membre");
		this.listeMembres = listeMembres;
	}
	
	public void ajouterMembre(Personne personne) {
		this.listeMembres.add(personne);
	}
	/**
	 * @return the style
	 */
	public String getStyle() {
		return style;
	}
	/**
	 * @param style the style to set
	 * @throws CreationException 
	 */
	public void setStyle(String style) throws CreationException {
		if(style==null || style.trim().equals("")) throw new CreationException("Le style du groupe n'est pas indiqué");
		this.style = style;
	}
	
}
