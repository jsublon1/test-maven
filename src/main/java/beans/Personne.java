package beans;

import java.util.ArrayList;

import exceptions.CreationException;

public class Personne {

	int id;
	String nom, prenom, telephone, email;
	ArrayList<String> listeInstruments = new ArrayList<String>();
	ArrayList<String> listeTypes = new ArrayList<String>();
	Adresse adresse;
	
	public Personne(int id,String nom, String prenom, String telephone, String email, 
			ArrayList<String> instrument, Adresse adresse, ArrayList<String> listeTypes) throws CreationException{
		this.setId(id);
		this.setNom(nom);
		this.setPrenom(prenom);
		this.setTelephone(telephone);
		this.setEmail(email);
		this.setListeInstruments(instrument);
		this.setAdresse(adresse);
		this.setListeTypes(listeTypes);
	}
	public Personne(String nom, String prenom, String telephone, String email, 
			ArrayList<String> listeInstruments, Adresse adresse, ArrayList<String> listeTypes) throws CreationException{
		this.setId(0);
		this.setNom(nom);
		this.setPrenom(prenom);
		this.setTelephone(telephone);
		this.setEmail(email);
		this.setListeInstruments(listeInstruments);
		this.setAdresse(adresse);
		this.setListeTypes(listeTypes);
	}
	
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 * @throws CreationException 
	 */
	public void setId(int id) throws CreationException {
		if (id<0) throw new CreationException("L'identifiant est négatif");
		this.id = id;
	}


	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * @param nom the nom to set
	 * @throws CreationException 
	 */
	public void setNom(String nom) throws CreationException {
		if(nom.trim().isEmpty()||nom==null) 
			throw new CreationException("Le nom du membre n'est pas indiqué");
		this.nom = nom;
	}
	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}
	/**
	 * @param prenom the prenom to set
	 * @throws CreationException 
	 */
	public void setPrenom(String prenom) throws CreationException {
		if(prenom.trim().isEmpty()||prenom==null) 
			throw new CreationException("Le prénom du membre n'est pas indiqué");
		this.prenom = prenom;
	}
	/**
	 * @return the telephone
	 */
	public String getTelephone() {
		return telephone;
	}
	/**
	 * @param telephone the telephone to set
	 * @throws CreationException 
	 */
	public void setTelephone(String telephone) throws CreationException {
		if(telephone.trim().isEmpty()||telephone==null) 
			throw new CreationException("Le numéro de télephone n'est pas indiqué");
		if(!telephone.matches("(0|\\+33|0033)[1-9][0-9]{8}")) 
			throw new CreationException("Le numéro de télephone n'est pas correct");
		this.telephone = telephone;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 * @throws CreationException 
	 */
	public void setEmail(String email) throws CreationException {
		if(email.trim().isEmpty()||email==null) 
			throw new CreationException("L'email n'est pas indiqué");
		if(!email.matches("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$")) 
			throw new CreationException("L'email n'est pas correct");
		this.email = email;
	}
	/**
	 * @return the instrument
	 */
	
	/**
	 * @return the adresse
	 */
	public Adresse getAdresse() {
		return adresse;
	}
	/**
	 * @return the listeInstruments
	 */
	public ArrayList<String> getListeInstruments() {
		return listeInstruments;
	}
	/**
	 * @param listeInstruments the listeInstruments to set
	 */
	public void setListeInstruments(ArrayList<String> listeInstruments) {
		this.listeInstruments = listeInstruments;
	}
	/**
	 * @param adresse the adresse to set
	 * @throws CreationException 
	 */
	public void setAdresse(Adresse adresse) throws CreationException {
		if(adresse==null) 
			throw new CreationException("L'adresse n'est pas indiqué");
		this.adresse = adresse;
	}
	/**
	 * @return the listeTypes
	 */
	public ArrayList<String> getListeTypes() {
		return listeTypes;
	}
	/**
	 * @param listeTypes the listeTypes to set
	 */
	public void setListeTypes(ArrayList<String> listeTypes) {
		this.listeTypes = listeTypes;
	}
	
	
	
}
