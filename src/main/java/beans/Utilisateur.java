package beans;

import java.time.LocalDate;

import exceptions.CreationException;
import exceptions.DAOException;

public class Utilisateur {

	int id;
	String login,motDePasse, role, email, cookieToken;
	LocalDate cookieTokenExpiration;
	
	
	public Utilisateur(int id, String role,String pLogin, String pMotDePasse, 
			String email, String cookieToken, LocalDate cookieTokenExpiration) throws CreationException, DAOException {
		this.setId(id);
		this.setRole(role);
		this.setLogin(pLogin);
		this.setMotDePasse(pMotDePasse);
		this.setEmail(email);
		this.setCookieToken(cookieToken);
		this.setCookieTokenExpiration(cookieTokenExpiration);
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 * @throws CreationException 
	 */
	public void setId(int id) throws CreationException {
		if (id<0) throw new CreationException("L'identifiant est négatif");
		this.id = id;
	}
	
	/**
	 * @return the role
	 */
	public String getRole() {
		return role;
	}
	/**
	 * @param role the role to set
	 * @throws CreationException 
	 */
	public void setRole(String role) throws CreationException {
		if(role.trim().isEmpty()||role==null) 
			throw new CreationException("Le role de l'utilisateur n'est pas indiqué");
		this.role = role;
	}
	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * @param login the login to set
	 * @throws CreationException 
	 */
	public void setLogin(String login) throws CreationException {
		if(login.trim().isEmpty()||login==null) 
			throw new CreationException("Le login n'est pas indiqué");
		this.login = login;
	}

	/**
	 * @return the motDePasse
	 */
	public String getMotDePasse() {
		return motDePasse;
	}

	/**
	 * @param motDePasse the motDePasse to set
	 * @throws CreationException 
	 */
	public void setMotDePasse(String motDePasse) throws CreationException {
		if(motDePasse.trim().isEmpty()||motDePasse==null) 
			throw new CreationException("Le mot de passe n'est pas indiqué");
		this.motDePasse = motDePasse;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 * @throws CreationException 
	 */
	public void setEmail(String email) throws CreationException {
		if(email.trim().isEmpty()||email==null) 
			throw new CreationException("L'email n'est pas indiqué");
		if(!email.matches("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$")) 
			throw new CreationException("L'email n'est pas correct");
		this.email = email;
	}
	/**
	 * @return the cookieToken
	 */
	public String getCookieToken() {
		return cookieToken;
	}
	/**
	 * @param cookieToken the cookieToken to set
	 */
	private void setCookieToken(String cookieToken) {
		this.cookieToken = cookieToken;
	}
	/**
	 * @return the cookieTokenExpiration
	 */
	public LocalDate getCookieTokenExpiration() {
		return cookieTokenExpiration;
	}
	/**
	 * @param cookieTokenExpiration the cookieTokenExpiration to set
	 * @throws DAOException 
	 */
	private void setCookieTokenExpiration(LocalDate cookieTokenExpiration) throws DAOException {
		if(this.getCookieToken()!=null && cookieTokenExpiration==null) 
			throw new DAOException("la date d'expiration du token ne peut pas être nulle");
		if(this.getCookieToken()==null) this.cookieTokenExpiration=null;
		else this.cookieTokenExpiration = cookieTokenExpiration;
	}
	
	public void setCookieToken(String pNom, LocalDate pDate) throws DAOException {
		if((pNom==null||pNom.trim().isEmpty())&&pDate!=null) throw new DAOException("Le token n'est pas indiqué");
		this.setCookieToken(pNom);
		this.setCookieTokenExpiration(pDate);
	
	}
}
