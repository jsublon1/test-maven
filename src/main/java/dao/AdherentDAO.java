package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import beans.Adresse;
import beans.Personne;
import exceptions.CreationException;
import exceptions.DAOException;
import static dao.DAOController.*;

public class AdherentDAO extends DAO<Personne> {

	@Override
	public boolean creer(Personne obj) throws SQLException, DAOException, CreationException {

		boolean transactionOK = false;
		int reqOK=0, id=0;
		this.connexion.setAutoCommit(false);
		
		int idAdresse =daoAdresse.verifAdresse(obj.getAdresse());
		if(idAdresse==0) {
			daoAdresse.creer(obj.getAdresse());
			idAdresse=UtilitairesDAO.getLastID("adresse");
		}

		StringBuilder creerAdherent = new StringBuilder();
		creerAdherent.append("INSERT INTO adherent ");
		creerAdherent.append("(adresse_id, ecole_id, ");
		creerAdherent.append("adherent_nom, adherent_prenom, ");
		creerAdherent.append("adherent_email, adherent_telephone) ");
		creerAdherent.append("values (?, ?, ?, ?, ?, ?)");

		

		PreparedStatement requeteCreer = this.connexion
				.prepareStatement(creerAdherent.toString());
		requeteCreer.setInt(1, idAdresse);
		requeteCreer.setInt(2, 1);
		requeteCreer.setString(3, obj.getNom());
		requeteCreer.setString(4, obj.getPrenom());
		requeteCreer.setString(5, obj.getEmail());
		requeteCreer.setString(6, obj.getTelephone());
		reqOK=requeteCreer.executeUpdate();
		if(reqOK==0) {
			this.connexion.rollback();
			return false;
		}
		transactionOK=true;
		id = UtilitairesDAO.getLastID("adherent");


		for(String instru : obj.getListeInstruments()) {
			if(transactionOK==true) {
				transactionOK=inserer(id, instru, "instrument", "joue");
			}
			else {
				this.connexion.rollback();
				return false;
			}
		}

		for(String type : obj.getListeTypes()) {
			if(transactionOK==true) {
				transactionOK=inserer(id, type, "type", "est");
			}
			else {
				this.connexion.rollback();
				return false;
			}
		}
		if (transactionOK==true) this.connexion.commit();
		
		return transactionOK;
	}

	public boolean inserer(int pId, String nom, String colonne, String table) throws SQLException {
		
		int reqOK=0;
		StringBuilder inserer = new StringBuilder();
		inserer.append("insert into "+table+" ");
		inserer.append("(adherent_id, "+colonne+"_id) ");
		inserer.append("values (?, ?)");
		
	
		PreparedStatement requete = this.connexion.prepareStatement(inserer.toString());
		requete.setInt(1, pId);
		requete.setInt(2, UtilitairesDAO.chercherTable(nom, colonne));
		reqOK=requete.executeUpdate();
		if(reqOK==0) return false;
		else return true;
		
		
	}
	@Override
	public boolean effacer(Personne obj) throws SQLException, DAOException {
		
		boolean transactionOK=false;
		transactionOK = UtilitairesDAO.effacerTable(obj.getId(), "adherent", "adherent");
		return transactionOK;
		
	}

	@Override
	public boolean modifier(Personne obj) throws SQLException, DAOException, CreationException {
		
		boolean transactionOK=false;
		this.connexion.setAutoCommit(false);
		StringBuilder modifier = new StringBuilder();
		modifier.append("update adherent ");
		modifier.append("set adresse_id =?, ");
		modifier.append(" ecole_id =?, ");
		modifier.append(" adherent_nom =?, ");
		modifier.append(" adherent_prenom =?, ");
		modifier.append(" adherent_email =?, ");
		modifier.append(" adherent_telephone =? ");
		modifier.append("where adherent_id = ? ");
		
		Adresse adresse = modifierAdresse(obj.getAdresse());
		
		PreparedStatement requete = this.connexion.prepareStatement(modifier.toString());
		requete.setInt(1, adresse.getId());
		requete.setInt(2, 1);
		requete.setString(3, obj.getNom());
		requete.setString(4, obj.getPrenom());
		requete.setString(5, obj.getEmail());
		requete.setString(6, obj.getTelephone());
		requete.setInt(7, obj.getId());
		
		int reqOK = requete.executeUpdate();
		if(reqOK==0) {
			this.connexion.rollback();
			return false;
		}
		transactionOK=true;
		
		transactionOK=UtilitairesDAO.effacerTable(obj.getId(), "adherent", "joue");
		if(transactionOK==false) {
			this.connexion.rollback();
			return false;
		}
		transactionOK=UtilitairesDAO.effacerTable(obj.getId(), "adherent", "est");
		if(transactionOK==false) {
			this.connexion.rollback();
			return false;
		}
		
		for(String instru : obj.getListeInstruments()) {
			if(transactionOK==true) {
				transactionOK=inserer(obj.getId(), instru, "instrument", "joue");
			}
			else {
				this.connexion.rollback();
				return false;
			}
		}
	
		for(String type : obj.getListeTypes()) {
			if(transactionOK==true) {
				transactionOK=inserer(obj.getId(), type, "type", "est");
			}
			else {
				this.connexion.rollback();
				return false;
			}
		}
		if (transactionOK==true) this.connexion.commit();
		
		return transactionOK;
		
	}

	@Override
	public Personne chercher(Integer pId) throws SQLException, DAOException, CreationException {
		
		AdresseDAO daoAdresse = new AdresseDAO();
		StringBuilder chercherAdherent = new StringBuilder();
		chercherAdherent.append("SELECT * FROM adherent ");
		chercherAdherent.append("WHERE adherent_id = ?");

		Personne adherent = null;


		PreparedStatement requeteChercher = this.connexion
				.prepareStatement(chercherAdherent.toString());
		requeteChercher.setInt(1, pId);
		ResultSet res = requeteChercher.executeQuery();
		while (res.next()) {
			adherent = new Personne(pId,
						res.getString("adherent_nom"),
						res.getString("adherent_prenom"),
						res.getString("adherent_telephone"),
						res.getString("adherent_email"),
						chercherInstrumentMusicien(pId),
						daoAdresse.chercher(res.getInt("adresse_id")),
						chercherTypeMusicien(res.getInt("adherent_id")));
		}
		return adherent;

	}

	@Override
	public ArrayList<Personne> chercherTous() throws DAOException, SQLException, CreationException{
		
		AdresseDAO daoAdresse = new AdresseDAO();
		StringBuilder chercherAdherent = new StringBuilder();
		chercherAdherent.append("SELECT * FROM adherent ");
		
		ArrayList<Personne> list = new ArrayList<>();
		
	
			Statement requeteChercher = this.connexion.createStatement();
			
			ResultSet res = requeteChercher.executeQuery(chercherAdherent.toString());
			while (res.next())
			{
				Personne adherent = new Personne(res.getInt("adherent_id"),
						res.getString("adherent_nom"),
						res.getString("adherent_prenom"),
						res.getString("adherent_telephone"),
						res.getString("adherent_email"),
						chercherInstrumentMusicien(res.getInt("adherent_id")),
						daoAdresse.chercher(res.getInt("adresse_id")),
						chercherTypeMusicien(res.getInt("adherent_id")));
			
			
			list.add(adherent);
			}
			if(list.size()==0) return null;
			return list;
		
	}


	public ArrayList<String> chercherInstrumentMusicien(int pId) throws SQLException, DAOException {
		
		
		StringBuilder chercherRole = new StringBuilder();
		chercherRole.append("SELECT I.instrument_nom FROM joue J ");
		chercherRole.append("inner join instrument I ");
		chercherRole.append("on I.instrument_id = J.instrument_id ");
		chercherRole.append("WHERE J.adherent_id = ?");
		
		ArrayList<String> listeInstrument = new ArrayList<>();
		
	
		PreparedStatement requeteChercher = this.connexion
				.prepareStatement(chercherRole.toString());
		requeteChercher.setInt(1, pId);
		ResultSet res = requeteChercher.executeQuery();
		
		while (res.next()) {
			listeInstrument.add(res.getString("I.instrument_nom"));
		}
		if(listeInstrument.size()==0) return null;
		return listeInstrument;
		
		
	}
	public ArrayList<String> chercherTypeMusicien(int pId) throws SQLException, DAOException {
		
		
		StringBuilder chercherRole = new StringBuilder();
		chercherRole.append("SELECT T.type_nom FROM est E ");
		chercherRole.append("inner join type t ");
		chercherRole.append("on T.type_id = E.type_id ");
		chercherRole.append("WHERE E.adherent_id = ?");
		
		ArrayList<String> listeType = new ArrayList<>();
		

		PreparedStatement requeteChercher = this.connexion
				.prepareStatement(chercherRole.toString());
		requeteChercher.setInt(1, pId);
		ResultSet res = requeteChercher.executeQuery();
		
		while (res.next()) {
			listeType.add(res.getString("T.type_nom"));
		}
		if(listeType.size()==0) return null;
		return listeType;
	
		
	}

	private Adresse modifierAdresse(Adresse adresse) throws DAOException, SQLException, CreationException {
		int id = daoAdresse.verifAdresse(adresse);
		
		if(id==0) {
			daoAdresse.creer(adresse);
			id = UtilitairesDAO.getLastID("adresse");
			
		}
		adresse.setId(id);
		return adresse;
	}

}
