package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import beans.Adresse;
import exceptions.CreationException;
import exceptions.DAOException;


/**
 * Classe DAO pour l'adresse
 * @author JSublon
 *
 */
public class AdresseDAO extends DAO<Adresse> {

	@Override
	/**
	 * Ajout d'une adresse dans la BDD
	 * @param obj : adresse a ajouter
	 * @return statut de l'operation
	 */
	public boolean creer(Adresse obj) throws DAOException, SQLException, CreationException {
		
				StringBuilder creerAdresse = new StringBuilder();
				creerAdresse.append("INSERT INTO adresse ");
				creerAdresse.append("(ADRESSE_NUM, ADRESSE_VOIE, ");
				creerAdresse.append("ADRESSE_CODEPOSTAL, ADRESSE_VILLE) ");
				creerAdresse.append("values (?, ?, ?, ?)");
				
				PreparedStatement requeteCreer = this.connexion.prepareStatement(creerAdresse.toString());
				requeteCreer.setString(1,obj.getNumero());
				requeteCreer.setString(2, obj.getVoie());
				requeteCreer.setString(3, obj.getCodePostal());
				requeteCreer.setString(4, obj.getVille());	
				
				int reqOK = requeteCreer.executeUpdate();
				
				if(reqOK==0) return false;
				else return true;


	}

	@Override
	public boolean effacer(Adresse obj) throws SQLException {
		boolean transactionOK=false;
		transactionOK = UtilitairesDAO.effacerTable(obj.getId(), "adresse", "adresse");
		return transactionOK;
	}

	@Override
	public boolean modifier(Adresse obj) throws SQLException {
		
		StringBuilder modifier = new StringBuilder();
		modifier.append("update adresse ");
		modifier.append("set adresse_num = ? ");
		modifier.append(", adresse_voie = ? ");
		modifier.append(", adresse_codepostal = ? ");
		modifier.append(", adresse_ville = ? ");
		modifier.append("where adresse_id = ? ");
		
		PreparedStatement requete = this.connexion.prepareStatement(modifier.toString());
		requete.setString(1, obj.getNumero());
		requete.setString(2, obj.getVoie());
		requete.setString(3, obj.getCodePostal());
		requete.setString(4, obj.getVille());
		requete.setInt(5, obj.getId());
		
		int reqOK = requete.executeUpdate();
		
		if(reqOK==0) return false;
		else return true;
	}

	@Override
	/**
	 * creation d'une adresse a partir de la BDD
	 * @param pId : id de l'adresse
	 * @return adresse
	 * @throws SQLException 
	 */
	public Adresse chercher(Integer pId) throws DAOException, SQLException, CreationException {
		
		StringBuilder chercherAdresse = new StringBuilder();
		chercherAdresse.append("SELECT * FROM adresse ");
		chercherAdresse.append("WHERE ADRESSE_ID = ?");
		
		Adresse adresse = null;
		

			PreparedStatement requeteChercher = this.connexion.prepareStatement(chercherAdresse.toString());
			requeteChercher.setInt(1, pId);
			ResultSet res = requeteChercher.executeQuery();
			while (res.next())
			{
			adresse = new Adresse(pId, res.getString("ADRESSE_NUM"),
					res.getString("ADRESSE_VOIE"), res.getString("ADRESSE_CODEPOSTAL"),
					res.getString("ADRESSE_VILLE"));
			}
			return adresse;
	
	}

	@Override
	/**
	 * creation de la liste de toutes les adresses
	 * @return liste des adresses
	 * @throws DAOException 
	 * @throws SQLException 
	 */
	public ArrayList<Adresse> chercherTous() throws DAOException, SQLException, CreationException {
		
		StringBuilder chercherAdresse = new StringBuilder();
		chercherAdresse.append("SELECT * FROM adresse ");
		
		ArrayList<Adresse> list = new ArrayList<>();
		

		Statement requeteChercher = this.connexion.createStatement();
		
		ResultSet res = requeteChercher.executeQuery(chercherAdresse.toString());
		while (res.next())
		{
		Adresse adresse = new Adresse(res.getInt("ADRESSE_ID"), res.getString("ADRESSE_NUM"),
				res.getString("ADRESSE_VOIE"), res.getString("ADRESSE_CODEPOSTAL"),
				res.getString("ADRESSE_VILLE"));
		list.add(adresse);
		}
		if(list.size()==0) return null;
		return list;

	}
	
	/**
	 * fonction pour verifier qu'une adresse existe dans la BDD
	 * @param adresse : adresse recherchee
	 * @return : identifiant de l'adresse de la BDD
	 * @throws DAOException : erreur utilisation BDD
	 * @throws CreationException 
	 * @throws SQLException 
	 */
	public int verifAdresse(Adresse adresse) throws DAOException, SQLException, CreationException
	{
		int identifiant = 0;
		
		ArrayList<Adresse> listAdresse = chercherTous();
		for(Adresse tempAdresse : listAdresse)
		{
			if (adresse.getVille().toUpperCase().equals(tempAdresse.getVille().toUpperCase())
					&& adresse.getCodePostal().equals(tempAdresse.getCodePostal())
					&& adresse.getVoie().toUpperCase().equals(tempAdresse.getVoie().toUpperCase())
					&& adresse.getNumero().toUpperCase().equals(tempAdresse.getNumero().toUpperCase()))
			{
				
				identifiant = tempAdresse.getId();
			}
		}
		return identifiant;
	}
}
