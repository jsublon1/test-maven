package dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import exceptions.CreationException;
import exceptions.DAOException;

/**
 * Classe abstraite pour la creation des DAO
 * @author JSublon
 *
 * @param <T> : objet generique
 */
public abstract class DAO<T> {
	
	//connection à la base de données
	protected Connection connexion = Singleton.getInstanceDB();
	
	/**
	 * sauvegarde dans la base de donnees a partir d'un objet
	 * @param obj : objet JAVA
	 * @return resultat de l'operation
	 * @throws SQLException : erreur de base de donnees
	 * @throws DAOException : erreur dans l'utilisation de la base de donnees
	 * @throws CreationException 
	 * @throws EntreeException : erreur de saisie
	 */
	public abstract boolean creer(T obj) throws SQLException, DAOException, CreationException;
	
	/**
	 * supression dans la base de donnees a partir d'un objet
	 * @param obj : objet JAVA
	 * @return resultat de l'operation
	 * @throws SQLException : erreur de base de donnees
	 * @throws DAOException : erreur dans l'utilisation de la base de donnees
	 */
	public abstract boolean effacer(T obj) throws SQLException, DAOException;
	
	/**
	 * modification de la base de donnees a partir d'un objet
	 * @param obj : objet JAVA
	 * @return resultat de l'operation
	 * @throws SQLException : erreur de base de donnees
	 * @throws DAOException : erreur dans l'utilisation de la base de donnees
	 * @throws CreationException 
	 */
	public abstract boolean modifier(T obj) throws SQLException, DAOException, CreationException;
	
	/**
	 * creation d'un objet JAVA a partir de la BDD
	 * @param pId : identifiant dans la base de donnees
	 * @return objet
	 * @throws SQLException : erreur de base de donnees
	 * @throws DAOException : erreur dans l'utilisation de la base de donnees
	 * @throws CreationException 
	 */
	public abstract T chercher(Integer pId) throws SQLException, DAOException, CreationException;
	
	/**
	 * creation d'une liste a partir de la BDD
	 * @return liste d'objet
	 * @throws SQLException : erreur de base de donnees
	 * @throws DAOException : erreur dans l'utilisation de la base de donnees
	 * @throws CreationException 
	 */
	public abstract ArrayList<T> chercherTous() throws SQLException, DAOException, CreationException;

}
