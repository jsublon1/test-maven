package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

import beans.Ecole;
import beans.Utilisateur;
import exceptions.CreationException;
import exceptions.DAOException;

public class EcoleDAO extends DAO<Ecole> {

	@Override
	public boolean creer(Ecole obj) throws SQLException, DAOException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean effacer(Ecole obj) throws SQLException, DAOException {
		boolean transactionOK=false;
		transactionOK = UtilitairesDAO.effacerTable(obj.getId(), "ecole", "ecole");
		return transactionOK;
	}

	@Override
	public boolean modifier(Ecole obj) throws SQLException, DAOException {
		
		StringBuilder modifier = new StringBuilder();
		modifier.append("update ecole ");
		modifier.append("set ecole_nom = ? ");
		modifier.append(", ecole_email = ? ");
		modifier.append(", ecole_telephone = ? ");
		modifier.append("where ecole_id = ? ");
		
		PreparedStatement requete = this.connexion.prepareStatement(modifier.toString());
		requete.setString(1, obj.getNom());
		requete.setString(2, obj.getEmail());
		requete.setString(3, obj.getTelephone());
		requete.setInt(4, obj.getId());
		
		int reqOK = requete.executeUpdate();
		
		if(reqOK==0) return false;
		else return true;
	}

	@Override
	public Ecole chercher(Integer pId) throws SQLException, DAOException {
		
		StringBuilder chercherUtilisateur = new StringBuilder();
		chercherUtilisateur.append("SELECT * FROM ecole ");
		chercherUtilisateur.append("WHERE ECOLE_ID = ?");
		
		Ecole ecole = null;
		
		try {
			PreparedStatement requeteChercher = this.connexion.prepareStatement(chercherUtilisateur.toString());
			requeteChercher.setInt(1, pId);
			ResultSet res = requeteChercher.executeQuery();
			while (res.next())
			{

				ecole = new Ecole(pId, res.getString("ecole_nom"),
						res.getString("ecole_email"), res.getString("ecole_telephone"));


			}
			return ecole;
		} catch (SQLException  e) {
			System.out.println("erreur base de donnée : connexion adresse 3");
			return null;
		} 
	}

	@Override
	public ArrayList<Ecole> chercherTous() throws SQLException, DAOException {
		// TODO Auto-generated method stub
		return null;
	}

}
