package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;

import beans.Groupe;
import beans.Personne;
import exceptions.CreationException;
import exceptions.DAOException;
import static dao.DAOController.*;

public class GroupeDAO extends DAO<Groupe> {

	@Override
	public boolean creer(Groupe obj) throws SQLException, DAOException {
		
		boolean transactionOK=false;
		int id=0;
		this.connexion.setAutoCommit(false);
		StringBuilder creationGroupe = new StringBuilder();
		creationGroupe.append("insert into groupe(");
		creationGroupe.append("groupe_nom, ");
		creationGroupe.append("groupe_datecreation, ");
		creationGroupe.append("ecole_id, ");
		creationGroupe.append("style_id) ");
		creationGroupe.append("values (?, ?, ?, ?)");
		
		PreparedStatement requete = this.connexion.prepareStatement(creationGroupe.toString());
		requete.setString(1, obj.getNom());
		requete.setString(2, obj.getDateCreation().toString());
		requete.setInt(3, 1);
		requete.setInt(4, UtilitairesDAO.chercherTable(obj.getStyle(), "style"));
		
		int reqOK = requete.executeUpdate();
		
		
		if (reqOK==0) {
			this.connexion.rollback();
			return false;
		}
		else {
			transactionOK=true;
			id = UtilitairesDAO.getLastID("groupe");
			for(Personne personne : obj.getListeMembres()) {
				transactionOK=insererMembre(id, personne);
				if(transactionOK==false) {
					this.connexion.rollback();
					return false;
				}
			}
		}
		if(transactionOK==true)	this.connexion.commit();
		return transactionOK;
	}


	@Override
	public boolean effacer(Groupe obj) throws SQLException, DAOException {
		boolean transactionOK=false;
		transactionOK = UtilitairesDAO.effacerTable(obj.getId(), "groupe", "groupe");
		return transactionOK;
	}

	@Override
	public boolean modifier(Groupe obj) throws SQLException, DAOException {
		boolean transactionOK=false;
		this.connexion.setAutoCommit(false);
		
		transactionOK=UtilitairesDAO.effacerTable(obj.getId(), "groupe", "compose");
		if(transactionOK==false) {
			this.connexion.rollback();
			return false;
		}
		
		StringBuilder modifier = new StringBuilder();
		modifier.append("update groupe ");
		modifier.append("set style_id =?, ");
		modifier.append(" ecole_id =?, ");
		modifier.append(" groupe_nom =?, ");
		modifier.append(" groupe_datecreation =? ");
		modifier.append("where groupe_id = ? ");
		
		PreparedStatement requete = this.connexion.prepareStatement(modifier.toString());
		requete.setInt(1, UtilitairesDAO.chercherTable(obj.getStyle(), "style"));
		requete.setInt(2, 1);
		requete.setString(3, obj.getNom());
		requete.setString(4, obj.getDateCreation().toString());
		requete.setInt(5, obj.getId());
		
		int reqOK = requete.executeUpdate();
		if(reqOK==0) {
			this.connexion.rollback();
			return false;
		}
		transactionOK=true;

		for(Personne personne : obj.getListeMembres()) {
			transactionOK=insererMembre(obj.getId(), personne);
			if(transactionOK==false) {
				this.connexion.rollback();
				return false;
			}
		}

		if (transactionOK==true) this.connexion.commit();
		
		return transactionOK;
	}

	@Override
	public Groupe chercher(Integer pId) throws SQLException, DAOException, CreationException {
		
		Groupe groupe = null;
		
		StringBuilder chercher = new StringBuilder();
		chercher.append("select * from groupe G ");
		chercher.append("inner join style S ");
		chercher.append("on G.style_id = S.style_id ");
		chercher.append("where G.groupe_id = ? ");
		
		PreparedStatement requete = this.connexion.prepareStatement(chercher.toString());
		requete.setInt(1, pId);
		
		ResultSet res = requete.executeQuery();
		while(res.next()) {
			groupe = new Groupe(pId, res.getString("G.groupe_nom"), 
					LocalDate.parse(res.getString("G.groupe_datecreation")), 
					res.getString("S.style_nom"),
					chercherMembres(pId));
		}
		
		return groupe;
	}

	@Override
	public ArrayList<Groupe> chercherTous() throws SQLException, DAOException, CreationException {
		
		ArrayList<Groupe> liste = new ArrayList<>();
		
		StringBuilder chercher = new StringBuilder();
		chercher.append("select * from groupe G ");
		chercher.append("inner join style S ");
		chercher.append("on G.style_id = S.style_id ");
		
		Statement requete = this.connexion.createStatement();
		
		
		ResultSet res = requete.executeQuery(chercher.toString());
		while(res.next()) {
			Groupe groupe = new Groupe(res.getInt("groupe_id"), res.getString("G.groupe_nom"), 
					LocalDate.parse(res.getString("G.groupe_datecreation")), 
					res.getString("S.style_nom"),
					chercherMembres(res.getInt("groupe_id")));
			liste.add(groupe);
		}
		
		return liste;
	}

	public boolean insererMembre(int pId, Personne personne) throws SQLException {
		
		int reqOK=0;
		StringBuilder creationMembre = new StringBuilder();
		creationMembre.append("insert into compose(groupe_id, adherent_id) ");
		creationMembre.append("values (?, ?)");
		

		PreparedStatement requete = this.connexion.prepareStatement(creationMembre.toString());
		requete.setInt(1, pId);
		requete.setInt(2, personne.getId());
		reqOK=requete.executeUpdate();
		
		if(reqOK==0) return false;
		else return true;
	}
	
	private ArrayList<Personne> chercherMembres(int pId) throws SQLException, DAOException, CreationException{
		
		ArrayList<Personne> list = new ArrayList<>();
		StringBuilder chercher = new StringBuilder();
		chercher.append("select adherent_id from compose ");
		chercher.append("where groupe_id = ?");
		
		PreparedStatement requete = this.connexion.prepareStatement(chercher.toString());
		requete.setInt(1, pId);
		
		ResultSet res = requete.executeQuery();
		while (res.next()) {
			Personne personne = daoAdherent.chercher(res.getInt("adherent_id"));
			list.add(personne);
		}
		
		if(list.size()==0) return null;
		else return list;
	}
}
