package dao;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
/**
 * Classe de connexion a la base de donnee
 * @author JSublon
 *
 */
public class Singleton {
	
	private static final String PATHCONF = "conf.properties";
	private static final String PROPERTY_URL = "jdbc.url";
	private static final String PROPERTY_USER = "jdbc.login";
	private static final String PROPERTY_PASSWORD = "jdbc.password";
	private static final String PROPERTY_DRIVER = "jdbc.driver";
	String url, driver, username, password;
	
	private static final Properties props = new Properties();
	private static Connection connexion;
	
	/**
	 * Constructeur de la connexion
	 */
	private Singleton()
	{
		try {

			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
	        InputStream file = classLoader.getResourceAsStream( PATHCONF );
			props.load(file);
            url = props.getProperty( PROPERTY_URL );
            driver = props.getProperty( PROPERTY_DRIVER );
            username = props.getProperty( PROPERTY_USER );
            password = props.getProperty( PROPERTY_PASSWORD );
            Class.forName(driver);
	
			
            connexion = DriverManager.getConnection(url, username, password);
			
		} catch (IOException | SQLException | ClassNotFoundException e) {
			System.out.println("erreur : " + e.getMessage());
		} 
	}

	/**
	 * getter de l'instance de connexion
	 * @return connexion a la BDD
	 */
	public static Connection getInstanceDB()
	{
		if (getConnection() == null)
		{
			new Singleton();
		}
		return getConnection();
	}
	
	/**
	 * Ferme la connexion a la BDD
	 */
	public static void closeInstanceDB()
	{
		try {
			Singleton.getConnection().close();
		} catch (SQLException e) {
			System.out.println("erreur DB : " + e.getMessage());
		}
	}

	/**
	 * getter de la connexion
	 * @return connexion a la BDD
	 */
	private static Connection getConnection() {
		return connexion;
	}
}
