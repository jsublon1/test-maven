package dao;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.time.LocalDate;
import java.util.ArrayList;

import beans.Utilisateur;
import exceptions.CreationException;
import exceptions.DAOException;

public class UserDAO extends DAO<Utilisateur> {

	@Override
	public boolean creer(Utilisateur obj) throws SQLException, DAOException {
	
			StringBuilder creerUtilisateur = new StringBuilder();
			creerUtilisateur.append("INSERT INTO admin ");
			creerUtilisateur.append("(role_id, admin_pseudo, ");
			creerUtilisateur.append("admin_motdepasse, admin_email, ");
			creerUtilisateur.append("admin_cookietoken, admin_cookietokenexpiration) ");
			creerUtilisateur.append("values (?, ?, ?, ?, ?, ?)");

			try {
				
				PreparedStatement requeteCreer = this.connexion.prepareStatement(creerUtilisateur.toString());
				requeteCreer.setInt(1,UtilitairesDAO.chercherTable(obj.getRole(),"role"));
				requeteCreer.setString(2, obj.getLogin());
				requeteCreer.setString(3, obj.getMotDePasse());
				requeteCreer.setString(4, obj.getEmail());	
				requeteCreer.setString(5, obj.getCookieToken());
				if (obj.getCookieTokenExpiration()!=null)
				requeteCreer.setString(6, obj.getCookieTokenExpiration().toString());
				else requeteCreer.setNull(6, Types.DATE);
				requeteCreer.executeUpdate();				
				return true;
			} catch (SQLException e) {
				System.out.println("erreur base de donnée : connexion adresse 1");
				return false;
			}
		
	
	}

	@Override
	public boolean effacer(Utilisateur obj) throws SQLException, DAOException {
		boolean transactionOK=false;
		transactionOK = UtilitairesDAO.effacerTable(obj.getId(), "admin", "admin");
		return transactionOK;
	}

	@Override
	public boolean modifier(Utilisateur obj) throws SQLException, DAOException {
		
		StringBuilder modifier = new StringBuilder();
		modifier.append("update admin ");
		modifier.append("set admin_pseudo = ? ");
		modifier.append(", role_id = ? ");
		modifier.append(", admin_motdepasse = ? ");
		modifier.append(", admin_email = ? ");
		modifier.append(", admin_cookietoken = ? ");
		modifier.append(", admin_cookietokenexpiration = ? ");
		modifier.append("where admin_id = ? ");
		
		PreparedStatement requete = this.connexion.prepareStatement(modifier.toString());
		requete.setString(1, obj.getLogin());
		requete.setInt(2, UtilitairesDAO.chercherTable(obj.getRole(), "role"));
		requete.setString(3, obj.getMotDePasse());
		requete.setString(4, obj.getEmail());
		requete.setString(5, obj.getCookieToken());
		if (obj.getCookieTokenExpiration()!=null)
			requete.setString(6, obj.getCookieTokenExpiration().toString());
			else requete.setNull(6, Types.DATE);
		requete.setInt(7, obj.getId());
		
		int reqOK = requete.executeUpdate();
		
		if(reqOK==0) return false;
		else return true;
	}

	@Override
	public Utilisateur chercher(Integer pId) throws SQLException, DAOException {
		
		StringBuilder chercherUtilisateur = new StringBuilder();
		chercherUtilisateur.append("SELECT * FROM admin A ");
		chercherUtilisateur.append("inner join role R ");
		chercherUtilisateur.append("ON R.role_id = A.role_id ");
		chercherUtilisateur.append("WHERE A.ADMIN_ID = ?");
		
		Utilisateur user = null;
		
		try {
			PreparedStatement requeteChercher = this.connexion.prepareStatement(chercherUtilisateur.toString());
			requeteChercher.setInt(1, pId);
			ResultSet res = requeteChercher.executeQuery();
			while (res.next())
			{
			if(res.getString("ADMIN_COOKIETOKENEXPIRATION")!=null) {
				user = new Utilisateur(pId, res.getString("R.role_nom"),
						res.getString("ADMIN_PSEUDO"), res.getString("ADMIN_MOTDEPASSE"),
						res.getString("ADMIN_EMAIL"),res.getString("ADMIN_COOKIETOKEN"),
						LocalDate.parse(res.getString("ADMIN_COOKIETOKENEXPIRATION")));
			}
			else {
				user = new Utilisateur(pId, res.getString("R.role_nom"),
						res.getString("ADMIN_PSEUDO"), res.getString("ADMIN_MOTDEPASSE"),
						res.getString("ADMIN_EMAIL"),res.getString("ADMIN_COOKIETOKEN"),
						null);
			}
			}
			return user;
		} catch (CreationException e) {
			throw new DAOException(e.getMessage());
		} catch (SQLException  e) {
			System.out.println("erreur base de donnée : connexion adresse 3");
			return null;
		} 
	}

	@Override
	public ArrayList<Utilisateur> chercherTous()
			throws SQLException, DAOException {
		
		StringBuilder chercherUtilisateur = new StringBuilder();
		chercherUtilisateur.append("SELECT * FROM admin A ");
		chercherUtilisateur.append("inner join role R ");
		chercherUtilisateur.append("ON R.role_id = A.role_id ");
		
		ArrayList<Utilisateur> list = new ArrayList<>();
		
		try {
			Statement requeteChercher = this.connexion.createStatement();
			
			ResultSet res = requeteChercher.executeQuery(chercherUtilisateur.toString());
			while (res.next())
			{
				Utilisateur utilisateur = null;
			if(res.getString("ADMIN_COOKIETOKENEXPIRATION")!=null) {
				utilisateur = new Utilisateur(res.getInt("ADMIN_ID"),
						res.getString("R.role_nom"),
						res.getString("ADMIN_PSEUDO"), res.getString("ADMIN_MOTDEPASSE"),
						res.getString("ADMIN_EMAIL"),res.getString("ADMIN_COOKIETOKEN"),
						LocalDate.parse(res.getString("ADMIN_COOKIETOKENEXPIRATION")));	
			}
			else {
				utilisateur = new Utilisateur(res.getInt("ADMIN_ID"),
						res.getString("R.role_nom"),
						res.getString("ADMIN_PSEUDO"), res.getString("ADMIN_MOTDEPASSE"),
						res.getString("ADMIN_EMAIL"),res.getString("ADMIN_COOKIETOKEN"),
						null);
			}
			list.add(utilisateur);
			}
			return list;
		} catch (CreationException e) {
			throw new DAOException(e.getMessage());
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion adresse 5");
			return null;
		}
	}
	
public Utilisateur chercher(String login) throws SQLException, DAOException {
		
		StringBuilder chercherUtilisateur = new StringBuilder();
		chercherUtilisateur.append("SELECT * FROM admin A ");
		chercherUtilisateur.append("inner join role R ");
		chercherUtilisateur.append("ON R.role_id = A.role_id ");
		chercherUtilisateur.append("WHERE A.ADMIN_PSEUDO = ?");
		
		Utilisateur user = null;
		
		try {
			PreparedStatement requeteChercher = this.connexion.prepareStatement(chercherUtilisateur.toString());
			requeteChercher.setString(1, login);
			ResultSet res = requeteChercher.executeQuery();
			while (res.next())
			{
			if(res.getString("ADMIN_COOKIETOKENEXPIRATION")!=null) {
				user = new Utilisateur(res.getInt("A.ADMIN_ID"), res.getString("R.role_nom"),
						res.getString("ADMIN_PSEUDO"), res.getString("ADMIN_MOTDEPASSE"),
						res.getString("ADMIN_EMAIL"),res.getString("ADMIN_COOKIETOKEN"),
						LocalDate.parse(res.getString("ADMIN_COOKIETOKENEXPIRATION")));
			}
			else {
				user = new Utilisateur(res.getInt("A.ADMIN_ID"), res.getString("R.role_nom"),
						res.getString("ADMIN_PSEUDO"), res.getString("ADMIN_MOTDEPASSE"),
						res.getString("ADMIN_EMAIL"),null,
						null);
			}
			}
			return user;
		} catch (CreationException e) {
			throw new DAOException(e.getMessage());
		} catch (SQLException  e) {
			System.out.println("erreur base de donnée : connexion adresse 3");
			return null;
		} 
	}

}
