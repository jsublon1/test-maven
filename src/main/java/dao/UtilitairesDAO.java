package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class UtilitairesDAO {

	protected static Connection connexion = Singleton.getInstanceDB();

	public static String chercherTable(int pId, String table) {

		StringBuilder chercherInstrument = new StringBuilder();
		chercherInstrument.append("SELECT * FROM " + table + " ");
		chercherInstrument.append("WHERE " + table + "_id = ?");

		String instrument = null;

		try {
			PreparedStatement requeteChercher = connexion
					.prepareStatement(chercherInstrument.toString());
			requeteChercher.setInt(1, pId);
			ResultSet res = requeteChercher.executeQuery();
			while (res.next()) {
				instrument = res.getString(table + "_nom");
			}
			return instrument;
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion adresse 3");
			return null;
		}
	}

	public static int chercherTable(String nom, String table) {

		StringBuilder chercherInstrument = new StringBuilder();
		chercherInstrument.append("SELECT * FROM " + table + " ");
		chercherInstrument.append("WHERE " + table + "_nom = ?");

		int varId = 0;

		try {
			PreparedStatement requeteChercher = connexion
					.prepareStatement(chercherInstrument.toString());
			requeteChercher.setString(1, nom);
			ResultSet res = requeteChercher.executeQuery();
			while (res.next()) {
				varId = res.getInt(table + "_id");
			}
			return varId;
		} catch (SQLException e) {
			System.out.println("erreur base de donnée : connexion adresse 3");
			return 0;
		}
	}

	public static int getLastID(String table) throws SQLException {

		int lastID = 0;
		Statement stm = connexion.createStatement();
		ResultSet res = stm
				.executeQuery("SELECT last_insert_id() FROM " + table);

		while (res.next()) {
			lastID = res.getInt("last_insert_id()");
		}
		return lastID;
	}
	
	public static boolean effacerTable(int pID, String nomColonne, String nomTable) throws SQLException {
		
		StringBuilder effacer = new StringBuilder();
		effacer.append("delete from ");
		effacer.append(nomTable);
		effacer.append(" where ");
		effacer.append(nomColonne);
		effacer.append("_id = ?");
		
		PreparedStatement requete = connexion.prepareStatement(effacer.toString());
		requete.setInt(1, pID);
		
		int reqOK = requete.executeUpdate();
		if(reqOK==0) return false;
		else return true;
	}
	
	public static ArrayList<String> getListe( String table) throws SQLException {
		
		ArrayList<String> list = new ArrayList<>();
		String recherche = "SELECT * from "+table;
		
		Statement stm = connexion.createStatement();
		ResultSet res = stm.executeQuery(recherche);
		
		while(res.next()) {
			list.add(res.getString(table+"_nom"));
		}
		
		if(list.size()==0) return null;
		else return list;
	}
	

}
