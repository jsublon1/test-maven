package exceptions;

public class CreationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -209114810185280074L;

	public CreationException() {
		// TODO Auto-generated constructor stub
	}

	public CreationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CreationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public CreationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CreationException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
