package filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import utilitaires.TokenHelper;

/**
 * Servlet Filter implementation class CSRFFilter
 */
@WebFilter("/*")
public class CSRFFilter extends HttpFilter implements Filter {
       
	private final String URL_PATH = "index";
	
    /**
     * @see HttpFilter#HttpFilter()
     */
    public CSRFFilter() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest myRequest = (HttpServletRequest) request;
		HttpServletResponse myResponse = (HttpServletResponse) response;
		
		HttpSession session = myRequest.getSession();
		
		if(myRequest.getMethod().equals("POST")) {
			if(session.getAttribute("_csrf")!=null) {
				if(myRequest.getParameter(TokenHelper.CSRF_TOKEN_VALUE_NAME)!=null) {
					if(session.getAttribute("_csrf").equals(myRequest.getParameter(TokenHelper.CSRF_TOKEN_VALUE_NAME))) {
			
						chain.doFilter(myRequest, myResponse);
					}
					else {
						System.out.println("token non égaux");
						session.setAttribute("erreur", "requête non conforme");
						myResponse.sendRedirect(URL_PATH);
					}
				}
				else {
					System.out.println("pas de token dans requete");
					session.setAttribute("erreur", "requête non conforme");
					myResponse.sendRedirect(URL_PATH);
				}
			}
			else {
				System.out.println("pas de token dans la session");
				session.setAttribute("erreur", "requête non conforme");
				myResponse.sendRedirect(URL_PATH);
			}
			
		}
		else {
			chain.doFilter(myRequest, myResponse);
		}
		
		
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
