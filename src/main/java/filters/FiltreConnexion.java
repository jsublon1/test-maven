package filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class FiltreConnexion
 */
@WebFilter(filterName="FiltreConnexion",urlPatterns = {"/GestionMembre","/GestionGroupe"})
public class FiltreConnexion extends HttpFilter implements Filter {
       
    /**
	 * 
	 */
	private static final long serialVersionUID = -1500147230855512170L;

	/**
     * @see HttpFilter#HttpFilter()
     */
    public FiltreConnexion() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		
		HttpServletRequest myRequest = (HttpServletRequest) request;
		HttpServletResponse myResponse = (HttpServletResponse) response;
		
		HttpSession session = myRequest.getSession();
		
		if(session.getAttribute("utilisateur") == null) {
			myRequest.setAttribute("erreur", "Vous devez être connecté pour avoir accès à la page "
					+ myRequest.getServletPath().substring(1));
			session.setAttribute("URL", myRequest.getServletPath().substring(1));
			request.getRequestDispatcher("index").forward(myRequest, myResponse);

		}
		else {
			chain.doFilter(myRequest, myResponse);
		}
		
		
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
