package forms;

import static dao.DAOController.daoUser;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import beans.Utilisateur;
import exceptions.DAOException;
import utilitaires.Password;
public class LoginForm {
	
	public static Utilisateur verifLogin(String log, String mdp) {
		
		Utilisateur user = null;
		String mdpHash = "";
		try {
			user = daoUser.chercher(log);
		} catch (SQLException | DAOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(user!=null) {
			mdpHash = Password.encrypPassword(mdp, Password.getSalt(user.getMotDePasse()));
			if(!mdpHash.equals(user.getMotDePasse())) {
				user=null;
			}
		}

		return user;
	}
	
	public static String[] getCookies(String name, HttpServletRequest request) {
		
		Cookie[] cookies = request.getCookies();
		String[] arr=null;
		if(cookies!=null) {
			for (Cookie cookie : cookies) {
				if(cookie.getName().equals(name)) {
					arr = cookie.getValue().split("\\$");
				}
			}
		}
		
		return arr;
	}
	
	public static Utilisateur verifCookie(HttpServletRequest request) throws SQLException, DAOException {
		
		Utilisateur user = null;
		
		String[] cookie=getCookies("auth-token", request);
		if(cookie!=null) {
			user = daoUser.chercher(cookie[0]);
			String token = user.getCookieToken();

			LocalDate dateToken = user.getCookieTokenExpiration();
			if(user!=null) {
				if(token==null||!token.equals(cookie[1])) {
					user=null;
				}
				else {
					if(dateToken.isBefore(LocalDate.now()))
					{
						user=null;
					}
				}
			}
		}
		return user;
		
	}
	
	public static HttpSession logAuto(HttpServletRequest request) throws SQLException, DAOException {
		
		Utilisateur user = verifCookie(request);
		HttpSession session = request.getSession();
		if(user!=null) {
			session.setAttribute("utilisateur", user);
		}
		return session;
		
	}
	
	
	
}
