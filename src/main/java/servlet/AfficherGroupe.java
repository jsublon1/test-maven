package servlet;

import static dao.DAOController.daoAdherent;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Groupe;
import beans.Personne;
import exceptions.CreationException;
import exceptions.DAOException;
import forms.LoginForm;

import static dao.DAOController.*;
/**
 * Servlet implementation class AfficherGroupe
 */
@WebServlet("/AfficherGroupe")
public class AfficherGroupe extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final String URL_AFFICHER_GROUPE = "/WEB-INF/JSP/AfficherGroupe.jsp";
	private final String URL_PATH = "AfficherGroupe";
	private ArrayList<Groupe> listeGroupes = new ArrayList<>();   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AfficherGroupe() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			session=LoginForm.logAuto(request);
		} catch (SQLException | DAOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		session.setAttribute("URLPath", "AfficherGroupe");
		try {
			listeGroupes=daoGroupe.chercherTous();
			request.setAttribute("listeGroupes", listeGroupes);
			if(request.getParameter("txtGroupe")!=null&&!request.getParameter("txtGroupe").isEmpty()) {
				int id = Integer.parseInt(request.getParameter("txtGroupe"));
				Groupe groupe = daoGroupe.chercher(id);
				request.setAttribute("groupe", groupe);
				listeGroupes=daoGroupe.chercherTous();
			}
		} catch (DAOException | SQLException | CreationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		this.getServletContext().getRequestDispatcher(URL_AFFICHER_GROUPE).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.setAttribute("URLPath", URL_PATH);
		int id = Integer.parseInt(request.getParameter("txtGroupe"));
		try {
			Groupe groupe = daoGroupe.chercher(id);
			request.setAttribute("groupe", groupe);
			listeGroupes=daoGroupe.chercherTous();
			request.setAttribute("listeGroupes", listeGroupes);
			this.getServletContext().getRequestDispatcher(URL_AFFICHER_GROUPE).forward(request, response);
		} catch (SQLException | DAOException | CreationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
