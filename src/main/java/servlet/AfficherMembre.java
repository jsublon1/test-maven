package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Personne;
import exceptions.CreationException;
import exceptions.DAOException;

import static dao.DAOController.*;
/**
 * Servlet implementation class AfficherMembre
 */
@WebServlet("/AfficherMembre")
public class AfficherMembre extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final String URL_AFFICHER_MEMBRE = "/WEB-INF/JSP/AfficherMembre.jsp";
	private ArrayList<Personne> listeMembres = new ArrayList<>();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AfficherMembre() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.setAttribute("URLPath", "AfficherMembre");
		try {
			listeMembres=daoAdherent.chercherTous();
			request.setAttribute("listeMembres", listeMembres);
			if(request.getParameter("txtMembre")!=null&&!request.getParameter("txtMembre").isEmpty()) {
				int id = Integer.parseInt(request.getParameter("txtMembre"));
				Personne membre = daoAdherent.chercher(id);
				request.setAttribute("membre", membre);
				listeMembres=daoAdherent.chercherTous();
			}
		} catch (DAOException | SQLException | CreationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.getServletContext().getRequestDispatcher(URL_AFFICHER_MEMBRE).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.setAttribute("URLPath", "AfficherMembre");
		int id = Integer.parseInt(request.getParameter("txtMembre"));
		try {
			Personne membre = daoAdherent.chercher(id);
			request.setAttribute("membre", membre);
			listeMembres=daoAdherent.chercherTous();
			request.setAttribute("listeMembres", listeMembres);
			this.getServletContext().getRequestDispatcher(URL_AFFICHER_MEMBRE).forward(request, response);
		} catch (SQLException | DAOException | CreationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
