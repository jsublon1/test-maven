package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Utilisateur;
import exceptions.CreationException;
import exceptions.DAOException;
import forms.LoginForm;
import utilitaires.Password;

import static dao.DAOController.*;
/**
 * Servlet implementation class CreerCompte
 */
@WebServlet("/CreerCompte")
public class CreerCompte extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private final String URL_INSCRIPTION = "/WEB-INF/JSP/Inscription.jsp";
    private String erreur="";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreerCompte() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		this.getServletContext().getRequestDispatcher(URL_INSCRIPTION).forward(request, response);
		erreur="";
		HttpSession session = request.getSession();
		try {
			session=LoginForm.logAuto(request);
		} catch (SQLException | DAOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		session.removeAttribute("erreurLogin");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String email = request.getParameter("txtEmail");
		String pseudo = request.getParameter("txtPseudo");
		String password	= request.getParameter("txtMDP");
		String passwordConf = request.getParameter("txtMDPConfirm");
		
		request.setAttribute("email", email);
		request.setAttribute("pseudo", pseudo);
		
		HttpSession session = request.getSession();
		session.setAttribute("erreur", erreur);
		
		
		if(!password.equals(passwordConf)) {
			erreur="Les mots de passe sont différent";
			doGet(request, response);
		}
		if(pseudo!=null&&!pseudo.trim().isEmpty()) {
			if(email!=null&&!email.trim().isEmpty()) {
				if(password!=null&&!password.trim().isEmpty()) {
					
					password=Password.encrypPassword(password);
					try {
						Utilisateur user = new Utilisateur(0, "admin",pseudo, password, 
								email, null, null);
						daoUser.creer(user);
						response.sendRedirect("index");
					} catch (CreationException | DAOException | SQLException e) {
						erreur=e.getMessage();
						doGet(request, response);
					}
					
				}
				else
				{
					erreur="veuillez entrer un mot de passe";
					doGet(request,response);
				}
			}
			else {
				erreur="veuillez entrer une adresse mail";
				doGet(request,response);
			}
			
		}
		else {
			erreur="veuillez entrer un login";
			doGet(request,response);
		}
		
	}

}
