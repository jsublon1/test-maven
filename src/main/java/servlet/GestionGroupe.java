package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Personne;
import dao.UtilitairesDAO;
import exceptions.CreationException;
import exceptions.DAOException;
import forms.LoginForm;
import beans.Adresse;
import beans.Groupe;
import static dao.DAOController.*;
/**
 * Servlet implementation class Groupe
 */
@WebServlet("/GestionGroupe")
public class GestionGroupe extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final String URL_CREATION_GROUPE = "/WEB-INF/JSP/CreationGroupe.jsp";
    private final String URL_AFFICHER_GROUPE = "/WEB-INF/JSP/AfficherGroupe.jsp";
	
	ArrayList<String> listeStyles = new ArrayList<>();
	String erreur="";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GestionGroupe() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    public void init() {
    	
    	try {
			
			listeStyles=UtilitairesDAO.getListe("style");
		} catch (SQLException e) {
			erreur=e.getMessage();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		ArrayList<Personne> listeMembresGroupe = (ArrayList<Personne>) request.getAttribute("membres");
		ArrayList<String> listeId=(ArrayList<String>) request.getAttribute("listeIdMembres");
	
		HttpSession session = request.getSession();
		try {
			session=LoginForm.logAuto(request);
		} catch (SQLException | DAOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/**
		 * initilisation de la page modification en mettant tous les attributs de la session
		 * dans la requete
		 */
		if(session.getAttribute("etat")!=null&&session.getAttribute("etat").equals("Modification")) {
			String etat = "Modification";
			request.setAttribute("etat", etat);
			session.removeAttribute("etat");
			Groupe groupe = (Groupe) session.getAttribute("groupe");
			request.setAttribute("id", groupe.getId());
			request.setAttribute("NomGroupe", groupe.getNom());
			request.setAttribute("date", groupe.getDateCreation());
			request.setAttribute("styleGroupe", groupe.getStyle());
			listeMembresGroupe=groupe.getListeMembres();
			listeId=new ArrayList<>();
			for(Personne pers : listeMembresGroupe) {
				listeId.add(String.valueOf(pers.getId()));
			}
			request.setAttribute("membres", listeMembresGroupe);
			session.removeAttribute("groupe");
		}
		else if(request.getAttribute("etat")==null||!request.getAttribute("etat").equals("Modification")){
			String etat = "Creation";
			request.setAttribute("etat", etat);
		}
		
		
		ArrayList<Personne> listeTousMembres = new ArrayList<>();
		try {
			listeTousMembres=daoAdherent.chercherTous();
		} catch (DAOException | SQLException | CreationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		listeTousMembres=enleverItem(listeMembresGroupe, listeTousMembres);

		session.setAttribute("erreur", erreur);
		session.setAttribute("URLPath", "GestionGroupe");
		request.setAttribute("listeMembres", listeTousMembres);
		request.setAttribute("listeStyles", listeStyles);
		request.setAttribute("membres", listeMembresGroupe);
		request.setAttribute("listeIdMembres", listeId);
		this.getServletContext().getRequestDispatcher(URL_CREATION_GROUPE).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		LocalDate date = null;
		erreur = "";
		int id = 0;
		ArrayList<Personne> listeMembresGroupe = new ArrayList<>();
		ArrayList<String> listeId=new ArrayList<>();
		String btn = request.getParameter("btn");
		String nomGroupe = request.getParameter("txtNomGroupe");
		String styleGroupe = request.getParameter("txtStyle");
		String listeIdMembres = request.getParameter("listeIdMembres");
		String etat = request.getParameter("etat");
		if(!request.getParameter("txtDate").equals("")) {
			date = LocalDate.parse(request.getParameter("txtDate"));
			}
		if(listeIdMembres!=null&&!listeIdMembres.equals("[]"))
			{
				try {
					listeMembresGroupe=transformerStringEnArrayPersonne(listeIdMembres);
					listeId=transformerStringEnArray(listeIdMembres);
				}
				 catch (SQLException | DAOException | CreationException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		if(request.getParameter("txtId")!=null&&!request.getParameter("txtId").trim().isEmpty()) {
	    	id = Integer.parseInt(request.getParameter("txtId"));
	    }
		
		
		request.setAttribute("NomGroupe", nomGroupe);
		request.setAttribute("date", date);
		request.setAttribute("styleGroupe", styleGroupe);
		request.setAttribute("etat", etat);
		
		request.setAttribute("membres", listeMembresGroupe);
		request.setAttribute("listeIdMembres", listeId);
		if(btn.equals("Ajouter")) {
			if(request.getParameter("txtMembre")!=null&&!request.getParameter("txtMembre").trim().isEmpty()) {
				try {
					int idMembre = Integer.parseInt(request.getParameter("txtMembre"));
					listeMembresGroupe.add(daoAdherent.chercher(idMembre));
					listeId.add(String.valueOf(idMembre));
					this.doGet(request, response);
				} catch (CreationException | SQLException | DAOException e) {
					erreur = e.getMessage();
					request.setAttribute("erreur", erreur);
					this.doGet(request, response);
				}
			}
			else this.doGet(request, response);
			
					
		}
		
		else if(btn.equals("Creation")) {
			Groupe groupe;
			try {
				groupe = new Groupe(nomGroupe, date, styleGroupe,listeMembresGroupe);
				daoGroupe.creer(groupe);
				request.setAttribute("groupe", groupe);
				request.setAttribute("creation", "Groupe créé correctement");
				this.getServletContext().getRequestDispatcher( URL_AFFICHER_GROUPE).forward(request, response);
			} catch (CreationException | SQLException | DAOException e) {
				erreur = e.getMessage();
				request.setAttribute("erreur", erreur);
				this.doGet(request, response);
			}
		}
		else if(btn.equals("Modification")) {
			try {
				Groupe groupe = new Groupe(id, nomGroupe, date, styleGroupe, listeMembresGroupe);
				daoGroupe.modifier(groupe);
				request.setAttribute("groupe", groupe);
				request.setAttribute("creation", "Groupe modifié correctement");
				this.getServletContext().getRequestDispatcher(URL_AFFICHER_GROUPE).forward(request, response);
			} catch (CreationException | SQLException | DAOException e) {
				erreur = e.getMessage();
				request.setAttribute("erreur", erreur);
				this.doGet(request, response);
			}
		}
		else if(Integer.parseInt(btn)>0) {
			int idMembre = Integer.parseInt(btn);
			try {

				Personne pers = daoAdherent.chercher(idMembre);
				listeMembresGroupe=enleverItem(pers, listeMembresGroupe);
				listeId=enleverItem(String.valueOf(idMembre),listeId);
				this.doGet(request, response);
			} catch (SQLException | DAOException | CreationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	private ArrayList<Personne> enleverItem(ArrayList<Personne> petiteListe, ArrayList<Personne> grosseListe) {
		
		if(petiteListe!=null&&petiteListe.size()>0) {
			for(int i=0;i<grosseListe.size();i++) {
				for(int j=0;j<petiteListe.size();j++) {
					if(petiteListe.get(j).getId()==grosseListe.get(i).getId()) {
						grosseListe.remove(i);
					}
				}
			}
		}
		
		return grosseListe;
	}
	private ArrayList<Personne> enleverItem(Personne personne, ArrayList<Personne> grosseListe) {
		
		for(int i=0; i<grosseListe.size();i++) {
			if(personne.getId()==grosseListe.get(i).getId()) {
				grosseListe.remove(i);
			}
			
		}
		return grosseListe;
	}
	
	private ArrayList<String> enleverItem(String item, ArrayList<String> grosseListe) {
		
		for(int i=0; i<grosseListe.size();i++) {
			if(item.equals(grosseListe.get(i))) {
				grosseListe.remove(i);
			}
			
		}
		return grosseListe;
	}

	private ArrayList<Personne> transformerStringEnArrayPersonne(String liste) throws SQLException, DAOException, CreationException{
		ArrayList<Personne> array = new ArrayList<>();
		if(liste!=null&&!liste.trim().isEmpty()) {
			liste=liste.replace('[',' ');
			liste=liste.replace(']', ' ');
			liste=liste.trim();
			String[] arr = liste.split("\\,");
			
			for(String p : arr) {
				int id = Integer.parseInt(p.trim());
				array.add(daoAdherent.chercher(id));
			}
		}
		return array;
		
	}
	
	private ArrayList<String> transformerStringEnArray(String liste){
		ArrayList<String> array = new ArrayList<>();
		if(liste!=null&&!liste.trim().isEmpty()) {
			liste=liste.replace('[',' ');
			liste=liste.replace(']', ' ');
			liste=liste.trim();
			String[] arr = liste.split("\\,");
			
			for(String p : arr) {
				array.add(p.trim());
			}
		}
		return array;
		
	}

}
