package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Adresse;
import beans.Personne;
import dao.UtilitairesDAO;
import exceptions.CreationException;
import exceptions.DAOException;
import forms.LoginForm;

import static dao.DAOController.*;

/**
 * Servlet implementation class GestionMembre
 */
@WebServlet("/GestionMembre")
public class GestionMembre extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private final String URL_CREATION_MEMBRE = "/WEB-INF/JSP/CreationMembre.jsp";
    private final String URL_AFFICHER_MEMBRE = "/WEB-INF/JSP/AfficherMembre.jsp";
    String erreur = "";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GestionMembre() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    public void init() {
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		ArrayList<String> listeTypeMembre = (ArrayList<String>) request.getAttribute("listeTypeMembre");
	    ArrayList<String> listeInstrumentMembre = (ArrayList<String>) request.getAttribute("listeInstrumentMembre");
	    
		HttpSession session = request.getSession();
		if(session.getAttribute("etat")!=null&&session.getAttribute("etat").equals("Modification")) {
			String etat = "Modification";
			request.setAttribute("etat", etat);
			session.removeAttribute("etat");
			Personne pers = (Personne) session.getAttribute("adherent");
			request.setAttribute("id", pers.getId());
			request.setAttribute("nom", pers.getNom());
			request.setAttribute("prenom", pers.getPrenom());
			request.setAttribute("tel", pers.getTelephone());
			request.setAttribute("email", pers.getEmail());
			request.setAttribute("num", pers.getAdresse().getNumero());
			request.setAttribute("voie", pers.getAdresse().getVoie());
			request.setAttribute("cp", pers.getAdresse().getCodePostal());
			request.setAttribute("ville", pers.getAdresse().getVille());
			listeTypeMembre=pers.getListeTypes();
			listeInstrumentMembre=pers.getListeInstruments();
			request.setAttribute("listeTypeMembre", listeTypeMembre);
			request.setAttribute("listeInstrumentMembre", listeInstrumentMembre);
			session.removeAttribute("adherent");
		}
		else if(request.getAttribute("etat")==null||!request.getAttribute("etat").equals("Modification")){
			String etat = "Creation";
			request.setAttribute("etat", etat);
		}
		try {
			session=LoginForm.logAuto(request);
		} catch (SQLException | DAOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		session.setAttribute("erreur", erreur);
		session.setAttribute("URLPath", "GestionMembre");

		ArrayList<String> listeInstruments = new ArrayList<>();
		ArrayList<String> listeType = new ArrayList<>();
		try {
			listeInstruments=UtilitairesDAO.getListe("instrument");
			listeType=UtilitairesDAO.getListe("type");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		listeType=enleverItem(listeTypeMembre, listeType);
		listeInstruments=enleverItem(listeInstrumentMembre, listeInstruments);
		request.setAttribute("listeInstrument", listeInstruments);
		request.setAttribute("listeType", listeType);
		
		this.getServletContext().getRequestDispatcher(URL_CREATION_MEMBRE).forward(request, response);
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		int id = 0;
		String erreur = "";
		ArrayList<String> listeTypeMembre = new ArrayList<>();
	    ArrayList<String> listeInstrumentMembre = new ArrayList<>();
		
	    if(request.getParameter("txtId")!=null&&!request.getParameter("txtId").trim().isEmpty()) {
	    	id = Integer.parseInt(request.getParameter("txtId"));
	    }
	    
		String nom = request.getParameter("txtNom");
		String prenom = request.getParameter("txtPrenom");
		String tel = request.getParameter("txtTelephone");
		String email = request.getParameter("txtEmail");
		String num = request.getParameter("txtNumRue");
		String voie = request.getParameter("txtNomRue");
		String cp = request.getParameter("txtCP");
		String ville = request.getParameter("txtVille");
		String listeIns = request.getParameter("listeIns");
		String listeTyp = request.getParameter("listeTyp");
		String etat = request.getParameter("etat");
	
		if(listeTyp!=null&&!listeTyp.equals("[]"))
			listeTypeMembre=transformerStringEnArray(listeTyp);
		if(listeIns!=null&&!listeIns.equals("[]"))
			listeInstrumentMembre=transformerStringEnArray(listeIns);
		
		String btn = request.getParameter("btn");
		
		request.setAttribute("nom", nom);
		request.setAttribute("prenom", prenom);
		request.setAttribute("tel", tel);
		request.setAttribute("email", email);
		request.setAttribute("num", num);
		request.setAttribute("voie", voie);
		request.setAttribute("cp", cp);
		request.setAttribute("ville", ville);
		request.setAttribute("etat", etat);
		
		request.setAttribute("listeTypeMembre", listeTypeMembre);
		request.setAttribute("listeInstrumentMembre", listeInstrumentMembre);
		
		switch(btn) {
		case "ajouter un type" :
			if(!request.getParameter("txtType").isEmpty()) {
				
				listeTypeMembre.add(request.getParameter("txtType"));
				
			}
			this.doGet(request, response);
			break;
		case "ajouter un instrument" :
			if(!request.getParameter("txtInstrument").isEmpty()) {
				
				listeInstrumentMembre.add(request.getParameter("txtInstrument"));
				
			}
			this.doGet(request, response);
			break;
		case "Creation" :
			try {
				Adresse adresse = new Adresse(num, voie, cp, ville);
				Personne personne = new Personne(nom, prenom, tel, email, listeInstrumentMembre, adresse, listeTypeMembre);
				daoAdherent.creer(personne);
				request.setAttribute("membre", personne);
				request.setAttribute("creation", "Adhérent créé correctement");
				this.getServletContext().getRequestDispatcher(URL_AFFICHER_MEMBRE).forward(request, response);
			} catch (CreationException | SQLException | DAOException e) {
				erreur = e.getMessage();
				request.setAttribute("erreur", erreur);
				this.doGet(request, response);
			}
			break;
		case "Modification" :
			try {
				Adresse adresse = new Adresse(num, voie, cp, ville);
				Personne personne = new Personne(id,nom, prenom, tel, email, listeInstrumentMembre, adresse, listeTypeMembre);
				daoAdherent.modifier(personne);
				request.setAttribute("membre", personne);
				request.setAttribute("creation", "Adhérent modifié correctement");
				this.getServletContext().getRequestDispatcher(URL_AFFICHER_MEMBRE).forward(request, response);
			} catch (CreationException | SQLException | DAOException e) {
				erreur = e.getMessage();
				request.setAttribute("erreur", erreur);
				this.doGet(request, response);
			}
			break;
		default :
			listeTypeMembre.remove(btn);
			listeInstrumentMembre.remove(btn);
			this.doGet(request, response);
			break;
		}

	}
	
	private ArrayList<String> enleverItem(ArrayList<String> petiteListe, ArrayList<String> grosseListe) {
		
		if(petiteListe!=null) {
			for(String item : petiteListe) {
				grosseListe.remove(item);
			}
		}
		
		return grosseListe;
	}
	
	private ArrayList<String> enleverItem(String item, ArrayList<String> grosseListe) {
		
		for(int i=0; i<grosseListe.size();i++) {
			if(item.equals(grosseListe.get(i))) {
				grosseListe.remove(i);
			}
			
		}
		return grosseListe;
	}
	
	private ArrayList<String> transformerStringEnArray(String liste){
		ArrayList<String> array = new ArrayList<>();
		if(liste!=null&&!liste.trim().isEmpty()) {
			liste=liste.replace('[',' ');
			liste=liste.replace(']', ' ');
			liste=liste.trim();
			String[] arr = liste.split("\\,");
			
			for(String p : arr) {
				array.add(p.trim());
			}
		}
		return array;
		
	}

}
