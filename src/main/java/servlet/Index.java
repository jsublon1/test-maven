package servlet;

import static dao.DAOController.daoEcole;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Ecole;
import exceptions.DAOException;
import forms.LoginForm;
import utilitaires.Password;

/**
 * Servlet implementation class Index
 */
@WebServlet("/index")
public class Index extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private final String URL_ACCUEIL = "/WEB-INF/JSP/Accueil.jsp"; 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Index() {
        super();
        // TODO Auto-generated constructor stub
    }
    public void init() {
  

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			Ecole ecole = daoEcole.chercher(1);
			request.setAttribute("ecole", ecole);
		} catch (SQLException | DAOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		HttpSession session = request.getSession();
		try {
			session=LoginForm.logAuto(request);
		} catch (SQLException | DAOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		session.setAttribute("URLPath", "index");
		if(session.getAttribute("utilisateur")!=null && session.getAttribute("URL")!=null)
			{
			response.sendRedirect(session.getAttribute("URL").toString());
			session.removeAttribute("erreurLogin");
			}
		else {
			this.getServletContext().getRequestDispatcher(URL_ACCUEIL).forward(request, response);
			session.removeAttribute("erreurLogin");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request, response);		
	}
	

}
