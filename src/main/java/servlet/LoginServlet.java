package servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Utilisateur;
import exceptions.DAOException;
import forms.LoginForm;
import utilitaires.CookieHelper;
import static dao.DAOController.daoUser;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/Login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
			HttpSession session = request.getSession();
			try {
				session=LoginForm.logAuto(request);
		
			} catch (SQLException | DAOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
//		ServletContext servletContext = getServletContext();
		HttpSession session = request.getSession();
		String login = request.getParameter("txtLogin");
		String mdp = request.getParameter("txtPassword");
		
		if(login.isEmpty() || mdp.isEmpty()) {
			session.setAttribute("erreurLogin", "Veuillez remplir les champs");
		}
		else {
			Utilisateur user = LoginForm.verifLogin(login,mdp);
			if(user==null) {
				session.setAttribute("erreurLogin", "Utilisateur ou mot de passe incorrect");
				
			}
			else {
				session.setAttribute("utilisateur", user);
				session.removeAttribute("erreur");
				session.removeAttribute("erreurLogin");
				if(request.getParameterValues("rmb")!=null) {
					Cookie cookieAuth = CookieHelper.generateAuthCookie(login);
					String[] arr = cookieAuth.getValue().split("\\$");

					try {
						user.setCookieToken(arr[1], LocalDate.now().plusDays(1));
						daoUser.modifier(user);
						
					} catch (DAOException | SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					response.addCookie(cookieAuth);
					
				}				
			}
		}
		response.sendRedirect((String) session.getAttribute("URLPath"));
	}

}
