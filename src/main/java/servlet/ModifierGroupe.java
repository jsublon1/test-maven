package servlet;

import static dao.DAOController.daoAdherent;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Utilisateur;
import exceptions.CreationException;
import exceptions.DAOException;
import forms.LoginForm;
import static dao.DAOController.*;

/**
 * Servlet implementation class ModifierGroupe
 */
@WebServlet("/ModifierGroupe")
public class ModifierGroupe extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final String URL_CREATION_GROUPE = "/WEB-INF/JSP/CreationGroupe.jsp";
    private final String URL_AFFICHER_GROUPE = "/WEB-INF/JSP/AfficherGroupe.jsp";
    private final String SERVLET_MODIFICATION = "GestionGroupe";
    private final String SERVLET_AFFICHAGE = "AfficherGroupe";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModifierGroupe() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher(URL_AFFICHER_GROUPE).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String btn = request.getParameter("btn");
		int id = Integer.parseInt(request.getParameter("txtId"));
		switch(btn) {
			case "modifier" :
				session.setAttribute("etat", "Modification");
			try {
				session.setAttribute("groupe", daoGroupe.chercher(id));
				response.sendRedirect(SERVLET_MODIFICATION);
			} catch (SQLException | DAOException | CreationException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
				break;
			case "effacer" :
				request.setAttribute("effacer", "effacer");
				try {
					request.setAttribute("groupe", daoGroupe.chercher(id));
				} catch (SQLException | DAOException | CreationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				this.getServletContext().getRequestDispatcher(URL_AFFICHER_GROUPE).forward(request, response);
				break;
			case "confirmer" :
				String mdp = request.getParameter("txtPassword");
				Utilisateur user = (Utilisateur) session.getAttribute("utilisateur");
				user = LoginForm.verifLogin(user.getLogin(), mdp);
				
				if(user!=null) {
					try {
						daoGroupe.effacer(daoGroupe.chercher(id));

					} catch (SQLException | DAOException
							| CreationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
				response.sendRedirect(SERVLET_AFFICHAGE);
				break;
			case "enregistrer" :
				
				break;
		}
	}

}
