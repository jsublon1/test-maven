package utilitaires;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*
 * devra permettre la génération d'un cookie nommé auth-token ayant pour valeur un token d'une longueur de 50 caractères 
 * et d'une durée de vie  de 7 jours. Elle devra également avoir une méthode pour la destruction de ce cookie.
 */
public class CookieHelper {
	
	public static Cookie generateAuthCookie(String login) {
		
		Cookie cookie = new Cookie("auth-token",login+"$"+TokenHelper.createToken(50));
		cookie.setMaxAge(60*60*24);
		
		return cookie;
		
	}
	
	public static HttpServletRequest destroyAuthCookie(HttpServletRequest request) {
		
		Cookie[] listCookies = request.getCookies();
		if(listCookies!=null) {
			for (Cookie cookie : listCookies) {
				if(cookie.getName().equals("auth-token")) {
					cookie.setMaxAge(0);
				}
			}
		}
		return request;
	}

}
