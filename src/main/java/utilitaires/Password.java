package utilitaires;

import org.apache.commons.codec.digest.DigestUtils;

public class Password {

	final static String PEPPER = System.getenv("PEPPER");
	
	/**
	 * Encoder mot de passe sans sel
	 * @param password
	 * @return
	 */
	public static String encrypPassword(final String password) {
		
		String salt = TokenHelper.createToken(20);
		String passHash = hashPass(password, salt);
		System.out.println(passHash);
		return passHash;
	}
	/**
	 * Encoder mot de passe avec sel
	 * @param password
	 * @return
	 */
	public static String encrypPassword(final String password, String salt) {
		
		String passHash = hashPass(password, salt);
		return passHash;
	}
	
	public static String getSalt(String hashPass) {
		
		String[] arraySplit = hashPass.split("\\$");
		if(arraySplit.length<3) {
			System.out.println("impossible de récupérer le salt");
			return null;
		}
		else return arraySplit[1];
	}
	
	public static String hashPass(String password, String salt) {
		String sha256hexStr = DigestUtils.sha256Hex(password+salt+PEPPER);
		String passHash = String.format("sha256hex$%s$%s", salt,sha256hexStr);
		return passHash;
	}
}
