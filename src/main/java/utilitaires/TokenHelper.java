package utilitaires;

/*
 * devra permettre la génération d'un token aléatoire d'une longueur de 100 caractères. 
 * Elle devra également permettre de fournir une méthode permettant de générer un token nommé CSRF faisant appel 
 * à la méthode de génération de token aléatoire. Quand on fera appel à cette classe, on pourra alors soit demander un token CSRF, soit un token aléatoire de taille x.
 */
public class TokenHelper {
	
	public static String CSRF_TOKEN_VALUE_NAME = "csrf";

	public static String createToken(Integer taille) {
		
		String str ="ABCDEFGHIJKLMNOPQRSTUVWXYZ"+
				"abcdefghijklmnopqrstuvwxyz"+
				"0123456789";
		StringBuilder token = new StringBuilder();
		for(int i=0; i<taille;i++) {
			Integer index = (int) Math.floor(Math.random()*str.length());
			token.append(str.charAt(index));
			
		}
		return token.toString();
	}


}
