<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Accueil</title>
		<link rel="stylesheet" type="text/css" href="Style.css">
	</head>
	<body class='corps'>
	
		<c:import url="/WEB-INF/JSP/Login.jsp"></c:import>
		<h1>BIENVENUE</h1>
		<h2>A l'�cole de musique <c:out value='${ecole.nom}'/></h2>
		<p class="erreur"><c:out value='${erreur}'/></p>
		<footer>
			<h3>Contact : <c:out value='${ecole.email}'/></h3>
		</footer>
	</body>
</html>