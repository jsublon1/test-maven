<%@ page import="utilitaires.TokenHelper" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Informations Groupe</title>
		<link rel='stylesheet' type='text/css' href='Style.css'>
	</head>
	<body class="corps">
		<c:import url="/WEB-INF/JSP/Login.jsp"></c:import>
		<h1><c:out value='${creation}'/></h1>
		<c:if test="${not empty listeGroupes}">
			<form id='selectGroupe' method="post">
				<select onChange='rechargerPage()' name="txtGroupe" id="txtGroupe">
					<option value=''>-- Selectionnez un Groupe --</option>
					<c:forEach items='${listeGroupes}' var='groupe'>
						<option value='${groupe.id}'>${groupe.nom}</option>
					</c:forEach>
				</select>
			</form>
		</c:if>
		<c:if test="${not empty groupe }">
			<form id='formInfo'>
				<table>
					<thead>
						<tr>
							<th>Detail du groupe</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Nom du groupe</td>
							<td><c:out value="${groupe.nom}"/></td>
						</tr>
						<tr>
							<td>Style de musique</td>
							<td><c:out value='${groupe.style}'/></td>
						</tr>
						<tr>
							<td>Date de Cr�ation</td>
							<td><c:out value="${groupe.dateCreation}"/></td>
						</tr>
					</tbody>
				</table>
				<input type='hidden' name='txtId' value='${groupe.id}'>
				<c:if test='${empty creation && not empty sessionScope.utilisateur}'>
					<button form='formInfo' name='btn' value='modifier' 
						formaction="./ModifierGroupe" formmethod="post">Modifier</button>
					<button form='formInfo' type='submit' name='btn' value='effacer' 
						formaction="./ModifierGroupe" formmethod="post">Effacer</button>
				</c:if>
				<c:if test='${effacer=="effacer"}'>
					<input type='password' name='txtPassword' id='txtPassword' placeholder='entrez mot de passe pour confirmer'>
					<button form='formInfo' name='btn' value='confirmer' 
						formaction="./ModifierGroupe" formmethod="post">Confirmer</button>
				</c:if>
				<c:set var="csrf" value="${TokenHelper.createToken(20)}"/>
				<c:set var="_csrf" value="${csrf}" scope="session"/>
				<input type="hidden" name="${TokenHelper.CSRF_TOKEN_VALUE_NAME}" value="${csrf}">
			</form>
			<h4>Membres du groupe</h4>
			<ul>
				<c:forEach items="${groupe.listeMembres}" var="membre" varStatus="status">
					<li><c:out value="${membre.nom}"></c:out> <c:out
							value="${membre.prenom}"></c:out></li>
				</c:forEach>
			</ul>
			
		</c:if>
		
</body>
	<script>
		function rechargerPage(){
			document.getElementById('selectGroupe').action="./AfficherGroupe";
		     document.getElementById('selectGroupe').method = "GET";
		     document.getElementById('selectGroupe').submit();
		}
	</script>
</html>