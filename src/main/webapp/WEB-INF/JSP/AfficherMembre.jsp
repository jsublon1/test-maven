<%@ page import="utilitaires.TokenHelper" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Affichage Adh�rent</title>
		<link rel='stylesheet' type='text/css' href='Style.css'>
	</head>
	<body class="corps">
		<c:import url="/WEB-INF/JSP/Login.jsp"></c:import>
		<h1><c:out value='${creation}'/></h1>
		<c:if test="${not empty listeMembres}">
			<form id='selectMembre' method="post">
				<select onChange='rechargerPage()' name="txtMembre" id="txtMembre">
					<option value=''>-- Selectionnez un Adh�rent --</option>
					<c:forEach items='${listeMembres}' var='mem'>
						<option value='${mem.id}'>${mem.nom} ${mem.prenom}</option>
					</c:forEach>
				</select>
			</form>
		</c:if>
		<c:if test="${not empty membre}">
		
				<form id='formInfo' class='grille col-2 row-8'>
							
					<label for='txtPrenom'>Pr�nom</label>
					<input type='text' name='txtPrenom' id='txtPrenom' value='${membre.prenom}' disabled>
				
					<c:if test='${not empty sessionScope.utilisateur }'>
						<label for='txtNom'>Nom</label>
						<input type='text' name='txtNom' id='txtNom' value='${membre.nom}' disabled>
					
						<label for='txtTelephone'>T�l�phone</label>
						<input type='text' name='txtTelephone' id='txtTelephone' 
						value='${membre.telephone}' disabled>
					
					
						<label for='txtEmail'>Email</label>
						<input type='text' name='txtEmail' id='txtEmail' value='${membre.email}' disabled>
					
					
						<label for='txtAdresse'>Adresse</label>
						<div class='div-ligne'>
							<input type='text' name='txtNumRue' id='txtNumRue' 
							value='${membre.adresse.numero}' disabled>
							<input type='text' name='txtNomRue' id='txtNomRue' 
							value='${membre.adresse.voie}' disabled>
						</div>
						<label></label>
						<div class='div-ligne'>
							<input type='text' name='txtCP' id='txtCP' 
							value='${membre.adresse.codePostal}' disabled>
							<input type='text' name='txtVille' id='txtVille' 
							value='${membre.adresse.ville}' disabled>
						</div>
						
					</c:if>
				
					
	
					<label for='txtType'>Types</label>
					<div>
						<c:forEach items='${membre.listeTypes}' var='type'>
							<span><c:out value='${type}'/> / </span>
						</c:forEach>
					</div>
					
					<label for='txtInstrument'>Instruments jou�s</label>
					<div>
						<c:forEach items='${membre.listeInstruments}' var='inst'>
							<span><c:out value='${inst}'/> / </span>
						</c:forEach>
					</div>
					
					<input type='hidden' name='txtId' value='${membre.id}'>
					<c:if test='${empty creation && not empty sessionScope.utilisateur}'>
						<button form='formInfo' name='btn' value='modifier' 
							formaction="./ModifierMembre" formmethod="post">Modifier</button>
						<button form='formInfo' type='submit' name='btn' value='effacer' 
							formaction="./ModifierMembre" formmethod="post">Effacer</button>
					</c:if>
					<c:if test='${effacer=="effacer"}'>
						<input type='password' name='txtPassword' id='txtPassword' placeholder='entrez mot de passe pour confirmer'>
						<button form='formInfo' name='btn' value='confirmer' 
						formaction="./ModifierMembre" formmethod="post">Confirmer</button>
					</c:if>
					<c:set var="csrf" value="${TokenHelper.createToken(20)}"/>
					<c:set var="_csrf" value="${csrf}" scope="session"/>
					<input type="hidden" name="${TokenHelper.CSRF_TOKEN_VALUE_NAME}" value="${csrf}">
				</form>
			</c:if>

	</body>
	<script>
		function rechargerPage(){
			document.getElementById('selectMembre').action="./AfficherMembre";
		     document.getElementById('selectMembre').method = "GET";
		     document.getElementById('selectMembre').submit();
		}
	</script>
</html>