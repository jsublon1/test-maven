<%@ page import="utilitaires.TokenHelper" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Création d'un groupe</title>
		<link rel='stylesheet' type='text/css' href='Style.css'>
	</head>
	
	<body class='corps'>
		<c:import url="/WEB-INF/JSP/Login.jsp"></c:import>
		<h1><c:out value='${etat}'/> d'un groupe</h1>
		<p class="red"><c:out value='${erreur}'/></p>
		<section class='ligne'>
			<form method='post' id='formGroupe'>
				<div class='grille col-2 row-2'>
					<label for='txtNomGroupe'>Nom du groupe</label>
					<input type='text' name='txtNomGroupe' id='txtNomGroupe' 
						placeholder='saisir le nom du groupe'
						value='<c:out value="${NomGroupe}"/>'>
					<label for='txtDate'>Date de Création</label>
					<input type='date' name='txtDate' id='txtDate'
						value='<c:out value="${date}"/>'>
					<label for='txtStyle'>Style de musique</label>
					<select name='txtStyle' id='txtStyle'>
						<option value=''>-- Sélectionnez le style de musique</option>
						<c:forEach items='${listeStyles}' var='style'>
							<c:if test='${styleGroupe==style}'>
								<option value='${style}' selected>${style}</option>
							</c:if>
							<option value='${style}'>${style}</option>
						</c:forEach>
					</select>
				</div>
				<div class='grille col-3'>
					<label for='txtMembre'>Membre</label>
					<select name='txtMembre' id='txtMembre'>
						<option value='' selected>-- Sélectionnez un membre</option>
						<c:forEach items="${listeMembres}" var="mem">
							<option value='${mem.id}'>${mem.prenom} ${mem.nom}</option>
						</c:forEach>
					</select>
					<input type='submit' value='Ajouter' name='btn'>
				</div>

				<input type='submit' value='${etat}' name='btn'>
				
				<input type='hidden' name='etat' value='${etat}'/>
				<input type='hidden' name='txtId' value='${id}'/>
				<input type='text' name='listeIdMembres' value='${listeIdMembres}'>
				<c:set var="csrf" value="${TokenHelper.createToken(20)}"/>
				<c:set var="_csrf" value="${csrf}" scope="session"/>
				<input type="hidden" name="${TokenHelper.CSRF_TOKEN_VALUE_NAME}" value="${csrf}">
			</form>
			
			<div>
					<h2>Membres du groupe</h2>
					<ul>
						<c:forEach items="${membres}" var="membre" varStatus="status">
							<li>
								<span>
									<c:out value="${membre.nom}"></c:out>  
									<c:out value="${membre.prenom}"></c:out> 
									<button name='btn' value='${membre.id}' type='submit' form='formGroupe'>X</button>
								</span>
							</li>	
						</c:forEach>
					</ul>
				</div>
		</section>
		
	</body>
</html>