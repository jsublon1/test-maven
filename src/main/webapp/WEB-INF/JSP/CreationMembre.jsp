<%@ page import="utilitaires.TokenHelper" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Cr�ation d'un adh�rent</title>
		<link rel='stylesheet' type='text/css' href='Style.css'>
	</head>
	<body class='corps'>
		<c:import url="/WEB-INF/JSP/Login.jsp"></c:import>
		<h1><c:out value='${etat}'/> d'un adh�rent</h1>
		<p class='red'><c:out value="${erreur}"/></p>
		<form method='post' class='divForm' id='formMembre'>
				<div class='grille col-2 row-6'>
					<label for='txtNom'>Nom</label>
					<input type='text' name='txtNom' id='txtNom' placeholder='saisir le nom'
						value='<c:out value="${nom}"/>'>
				
			
					<label for='txtPrenom'>Pr�nom</label>
					<input type='text' name='txtPrenom' id='txtPrenom' placeholder='saisir le pr�nom'
						value='<c:out value="${prenom}"/>'>
				
				
					<label for='txtTelephone'>T�l�phone</label>
					<input type='text' name='txtTelephone' id='txtTelephone'
							 placeholder='saisir le num�ro de t�l�phone'
							 value='<c:out value="${tel}"/>'>
				
				
					<label for='txtEmail'>Email</label>
					<input type='text' name='txtEmail' id='txtEmail' placeholder="saisir l'email"
						value='<c:out value="${email}"/>'>
				
				
					<label for='txtAdresse'>Adresse</label>
					<div class='div-ligne'>
						<input type='text' name='txtNumRue' id='txtNumRue' placeholder='num�ro de rue'
							value='<c:out value="${num}"/>'>
						<input type='text' name='txtNomRue' id='txtNomRue' placeholder='nom de la rue'
							value='<c:out value="${voie}"/>'>
					</div>
					<label></label>
					<div class='div-ligne'>
						<input type='text' name='txtCP' id='txtCP' placeholder='code postal'
							value='<c:out value="${cp}"/>'>
						<input type='text' name='txtVille' id='txtVille' placeholder='ville'
							value='<c:out value="${ville}"/>'>
					</div>
				</div>
	
					<div>
						<h3>Type d'adh�rent</h3>
						<ul>
							<c:forEach items="${listeTypeMembre}" var="type" varStatus="status">
								<li>
									<c:out value='${type}'/>
									<button name='btn' value='${type}' type='submit' form='formMembre'>X</button>
								</li>
							</c:forEach>
						</ul>
					</div>
					<div>
						<h3>Instruments jou�s</h3>
						<ul>
							<c:forEach items="${listeInstrumentMembre}" var="instrument" varStatus="status">
								<li>
									<c:out value='${instrument}'/>
									<button name='btn' value='${instrument}' type='submit' form='formMembre'>X</button>
								</li>
							</c:forEach>
						</ul>
						<input type='hidden' name='listeIns' value='${listeInstrumentMembre}'>
						<input type='hidden' name='listeTyp' value='${listeTypeMembre}'>
		
				</div>
				<div class='grille col-3 row-2'>
					<label for='txtType'>Type d'adh�rent</label>
					<select name='txtType' id='txtType'>
						<option value='' selected>--Veuillez s�lectionner un type--</option>
						<c:forEach items="${listeType}" var="type" varStatus="status">
							<option value='<c:out value="${type}"/>'><c:out value='${type}'/></option>
						</c:forEach>
					</select>
					<input type='submit' name='btn' value='ajouter un type'/>
					<label for='txtInstrument'>Instrument jou�</label>
					<select name='txtInstrument' id='txtInstrument'>
						<option value='' selected>--Veuillez s�lectionner un instrument--</option>
						<c:forEach items="${listeInstrument}" var="ins" varStatus="status">
							<option value='<c:out value="${ins}"/>'><c:out value='${ins}'/></option>
						</c:forEach>
					</select>
					<input type='submit' name='btn' value='ajouter un instrument'/>
					
				</div>
				
				<input type='submit' value='${etat}' name='btn'>
				
				<input type='hidden' name='etat' value='${etat}'/>
				<input type='hidden' name='txtId' value='${id}'/>
				
				<c:set var="csrf" value="${TokenHelper.createToken(20)}"/>
				<c:set var="_csrf" value="${csrf}" scope="session"/>
				<input type="hidden" name="${TokenHelper.CSRF_TOKEN_VALUE_NAME}" value="${csrf}">
		</form>
	</body>
</html>