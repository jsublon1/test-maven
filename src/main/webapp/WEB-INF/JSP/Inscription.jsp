<%@ page import="utilitaires.TokenHelper" %>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="ISO-8859-1">
	<link rel='stylesheet' type='text/css' href='Style.css'/>
	<title>Inscription</title>
</head>
<body>
	<c:import url="/WEB-INF/JSP/Login.jsp"></c:import>
	<form class='grille col-2-large row-5' method='post'>
		<label for='txtEmail'>Adresse mail</label>
		<input type='text' name='txtEmail' id='txtEmail' value="${email}" placeholder="entrez votre adresse mail"/>
	
		<label for='txtPseudo'>Pseudo : </label>
		<input type='text' name='txtPseudo' id='txtPseudo' value="${pseudo}" placeholder="entrez votre pseudo"/>
		
		<label for='txtMDP'>Mot de Passe : </label>
		<input type='password' name='txtMDP' id='txtMDP' placeholder="entrez votre mot de passe"/>
		
		<label for='txtMDPConfirm'>Confirmer mot de Passe : </label>
		<input type='password' name='txtMDPConfirm' id='txtMDPConfirm' placeholder="confirmez votre mot de passe"/>
		
		<input type='submit' value="S'incrire" />
		<p class='red'><c:out value='${erreur}'/></p>
		
		<c:set var="csrf" value="${TokenHelper.createToken(20)}"/>
		<c:set var="_csrf" value="${csrf}" scope="session"/>
		<input type="hidden" name="${TokenHelper.CSRF_TOKEN_VALUE_NAME}" value="${csrf}">
	</form>
	
</body>
</html>