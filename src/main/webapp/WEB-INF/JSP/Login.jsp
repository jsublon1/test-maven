<%@ page import="utilitaires.TokenHelper" %>

<c:choose>
	<c:when test="${not empty sessionScope.utilisateur}">
	<nav>
		<form class='barreLogin' method='get' action='Logout'>
			<div>
				<a href="/ECF-ecole-musique/AfficherMembre">Infos Adh�rents</a>
				<a href="/ECF-ecole-musique/AfficherGroupe">Infos Groupes</a>
				<a href="/ECF-ecole-musique/GestionMembre">Cr�ation d'un
				Adh�rent</a>
				 <a href="/ECF-ecole-musique/GestionGroupe">Cr�ation
				d'un Groupe</a>
			</div>
			<div class='ligne'>
				<p><c:out value="${sessionScope.utilisateur.login}"></c:out></p>
				<input name='btnLogout' value='Log Out' type='submit'>
			</div>
		</form>
	</nav>
		
	</c:when>
	<c:otherwise>
	<nav>
		<form class='barreLogin' method='post' action="Login">
			<div>
					
				<a href="/ECF-ecole-musique/AfficherMembre">Infos Adh�rents</a>
				<a href="/ECF-ecole-musique/AfficherGroupe">Infos Groupes</a>
				<a href="/ECF-ecole-musique/CreerCompte">S'incrire</a>
			</div>
			<div>
				<div>
					<label for='txtLogin'>username : <input id='txtLogin'
					name='txtLogin' type='text' value='<c:out value="${login}" />'/></label> <label for='txtPassword'>password
					: <input id='txtPassword' name='txtPassword' type='password' />
					</label> <input name='btnConnect' value='Connexion' type='submit'>
				</div>
				<div class='ligne even'>
					<div class='ligne left'>
						<input type="checkbox" name="rmb" id="rmb">
						<label for='rmb'>Se souvenir de moi</label>
					</div>
					
					<p class='red'><c:out value='${erreurLogin}'/></p>
				</div>
				<c:set var="csrf" value="${TokenHelper.createToken(20)}"/>
				<c:set var="_csrf" value="${csrf}" scope="session"/>
				<input type="hidden" name="${TokenHelper.CSRF_TOKEN_VALUE_NAME}" value="${csrf}">
			</div>
		</form>
	</nav>
		
	</c:otherwise>
</c:choose>	
